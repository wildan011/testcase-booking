<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $payloads = [
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'phone' => '098908908',
                'address' => 'Bandung',
                'role' => 'admin',
                'password' => "Rahasia"
            ],
            [
                'name' => 'direksi',
                'email' => 'direksi@gmail.com',
                'phone' => '098908908',
                'address' => 'Bandung',
                'role' => 'direksi',
                'password' => "Rahasia"
            ]
        ];

        foreach ($payloads as $payload) {
            $user = User::where('email', $payload['email'])->first();
            if (!$user) {
                User::create($payload);
            }
        }
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
