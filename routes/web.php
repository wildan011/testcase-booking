<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Booking\BookingController;
use App\Http\Controllers\BookingClass\BookingClassController;
use App\Http\Controllers\DepartureSchedule\DepartureScheduleController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Vehicle\VehicleController;
use App\Http\Controllers\VehicleRoute\VehicleRouteController;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', [AuthController::class, 'loginForm'])->name("login");
Route::post('/login', [AuthController::class, 'login'])->name("post.login");
Route::get('/register', [AuthController::class, 'registerForm'])->name("register");
Route::post('/register', [AuthController::class, 'register'])->name("post.register");
Route::post('/logout', [AuthController::class, 'logout'])->name("logout");


Route::get('/home', [AuthController::class, 'loginForm'])->name("admin.home");

Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UserController::class, 'index'])->name('user.index');
    Route::get('/datatable', [UserController::class, 'datatable'])->name('user.datatable');
    Route::get('/store', [UserController::class, 'create'])->name('user.create');
    Route::post('/store', [UserController::class, 'store'])->name('user.store');
    Route::get('/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::put('/update/{id}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/destroy/{id}', [UserController::class, 'destroy'])->name('user.destroy');
});
Route::group(['prefix' => 'vehicles'], function () {
    Route::get('/', [VehicleController::class, 'index'])->name('vehicle.index');
    Route::get('/datatable', [VehicleController::class, 'datatable'])->name('vehicle.datatable');
    Route::get('/store', [VehicleController::class, 'create'])->name('vehicle.create');
    Route::post('/store', [VehicleController::class, 'store'])->name('vehicle.store');
    Route::get('/{id}', [VehicleController::class, 'edit'])->name('vehicle.edit');
    Route::put('/update/{id}', [VehicleController::class, 'update'])->name('vehicle.update');
    Route::delete('/destroy/{id}', [VehicleController::class, 'destroy'])->name('vehicle.destroy');
});

Route::group(['prefix' => 'vehicle_routes'], function () {
    Route::get('/', [VehicleRouteController::class, 'index'])->name('vehicle_route.index');
    Route::get('/datatable', [VehicleRouteController::class, 'datatable'])->name('vehicle_route.datatable');
    Route::get('/store', [VehicleRouteController::class, 'create'])->name('vehicle_route.create');
    Route::post('/store', [VehicleRouteController::class, 'store'])->name('vehicle_route.store');
    Route::get('/{id}', [VehicleRouteController::class, 'edit'])->name('vehicle_route.edit');
    Route::put('/update/{id}', [VehicleRouteController::class, 'update'])->name('vehicle_route.update');
    Route::delete('/destroy/{id}', [VehicleRouteController::class, 'destroy'])->name('vehicle_route.destroy');
});


Route::group(['prefix' => 'booking_classes'], function () {
    Route::get('/', [BookingClassController::class, 'index'])->name('booking_class.index');
    Route::get('/datatable', [BookingClassController::class, 'datatable'])->name('booking_class.datatable');
    Route::get('/store', [BookingClassController::class, 'create'])->name('booking_class.create');
    Route::post('/store', [BookingClassController::class, 'store'])->name('booking_class.store');
    Route::get('/{id}', [BookingClassController::class, 'edit'])->name('booking_class.edit');
    Route::put('/update/{id}', [BookingClassController::class, 'update'])->name('booking_class.update');
    Route::delete('/destroy/{id}', [BookingClassController::class, 'destroy'])->name('booking_class.destroy');
});

Route::group(['prefix' => 'departure_schedules'], function () {
    Route::get('/', [DepartureScheduleController::class, 'index'])->name('departure_schedule.view');
    Route::get('/{id}', [DepartureScheduleController::class, 'add'])->name('departure_schedule.add');
    Route::post('/store', [DepartureScheduleController::class, 'store'])->name('departure_schedule.store');
});

Route::group(['prefix' => 'bookings'], function () {
    Route::get('/', [BookingController::class, 'index'])->name('booking.index');
    Route::get('/{id}', [BookingController::class, 'add'])->name('booking.add');
    Route::post('/store', [BookingController::class, 'store'])->name('booking.store');
});
// });
