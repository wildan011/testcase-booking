<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <title>{{ config('app.name') }}</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="{{ asset('metronic/favicon.png') }}" />
    <link rel="stylesheet" href="{{ asset('metronic/font/font.css') }}" />
    <link href="{{ asset('metronic/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('metronic/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"
        id="plugins_css" />
    <link href="{{ asset('metronic/css/style.bundle.css') }}" rel="stylesheet" type="text/css" id="style_css" />
    <style>
        .aside.aside-dark,
        .aside.aside-dark .aside-logo,
        .aside-dark .menu .menu-item.hover:not(.here)>.menu-link:not(.disabled):not(.active):not(.here),
        .aside-dark .menu .menu-item:not(.here) .menu-link:hover:not(.disabled):not(.active):not(.here) {
            /* background: #081132; */
            background: #fff
        }

        .aside-dark .menu .menu-item.hover:not(.here)>.menu-link:not(.disabled):not(.active):not(.here) .menu-title {
            color: #444
        }

        .aside-dark .menu .menu-item:hover {
            color: #fff
        }

        .menu-link>span.menu-icon>span.svg-icon.svg-icon-2>svg path,
        a.menu-link:hover>span.menu-icon>span.svg-icon.svg-icon-2>svg path,
        a.menu-link.active>span.menu-icon>span.svg-icon.svg-icon-2>svg path {
            fill: var(--bs-primary) !important;
        }

        .menu-link:hover>span.menu-icon>span.svg-icon.svg-icon-2>svg path,
        a.menu-link:hover>span.menu-icon>span.svg-icon.svg-icon-2>svg path,
        a.menu-link.active>span.menu-icon>span.svg-icon.svg-icon-2>svg path {
            fill: var(--bs-primary) !important;
        }

        .menu-link:hover>span.menu-title {
            color: var(--bs-primary) !important;
        }

        .aside-dark .menu-link:hover {
            background-color: rgba(var(--bs-primary-rgb), 0.1) !important;
        }

        .aside-dark .menu .menu-item .menu-link.active .menu-title {
            color: var(--bs-primary) !important;
        }

        .aside-dark .menu .menu-item .menu-link .menu-title {
            color: #444;
        }

        .aside-dark .menu .menu-item .menu-link.active {
            background-color: rgba(var(--bs-primary-rgb), 0.1);
        }

        .page-item.active .page-link,
        .page-item.active .page-link:hover {
            background: rgba(var(--bs-primary-rgb), 0.9);
            color: white !important;
        }

        .page-link {
            color: var(--bs-primary);
        }

        .page-link:hover {
            color: var(--bs-primary) !important;
            background: rgba(var(--bs-primary-rgb), 0.1);
        }

        .form-required::after {
            content: " *";
            color: #f1416c;
            font-size: 12px;
        }

        a.menu-link:hover .bullet {
            color: var(--bs-primary);
            background-color: var(--bs-secondary) !important;
        }

        a.menu-link:hover .menu-title {
            color: var(--bs-primary) !important;
        }

        .dataTables_processing {
            color: white !important;
            font-size: 13px !important;
            font-weight: bold !important;
            background-color: #444 !important;
        }
    </style>
    @stack('stylesheet')
</head>

<body id="kt_body"
    class="page-loading-enabled page-loading header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">

    <!--begin::loader-->
    <div class="page-loader flex-column">
        <span class="spinner-border text-primary mb-3" role="status"></span>
        <span class="text-muted fs-6 fw-bold mt-5">Loading...</span>
    </div>
    <!--end::Loader-->

    <div class="d-flex flex-column flex-root">
        <div class="page d-flex flex-row flex-column-fluid">
            @include('templates.aside')
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper" style="padding-top: 50px">
                @include('templates.header')
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <div class="post d-flex flex-column-fluid" id="kt_post">
                        @include('templates.content')
                    </div>
                </div>
                @include('templates.footer')
            </div>
        </div>
    </div>

    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="currentColor" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="currentColor" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "assets/";
    </script>

    <script src="{{ asset('metronic/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('metronic/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script>
        @foreach (['success', 'info', 'error', 'warning', 'question'] as $key)
            @if ($message = Session::get($key))
                Swal.fire({
                    title: '{{ Session::get('title') }}',
                    html: '{!! $message !!}',
                    icon: '{{ $key }}',
                });
            @endif
        @endforeach
    </script>

    @stack('script')
</body>

</html>