@extends('templates.index')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    Form Kendaraan
                </div>
            </div>
        </div>
    </div>
    <form action="{{ @$vehicle ? route('vehicle.update', ['id'=>$vehicle->id]) : route('vehicle.store')}}"
        method="POST">
        @csrf
        @if (@$vehicle)
        @method('PUT')
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Nama Kendaraan</label>
                        <div class="col-md-8">
                            <input type="text" name="name" id="name"
                                class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name', @$vehicle['name']) }}">
                            @error('name')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Tipe</label>
                        <div class="col-md-8">
                            <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                                <option value="suv">SUV</option>
                                <option value="mpv">MPV</option>
                                <option value="croosver">Croosver</option>
                            </select>
                            @error('type')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Plat</label>
                        <div class="col-md-8">
                            <input type="text" name="plat" id="plat"
                                class="form-control @error('plat') is-invalid @enderror"
                                value="{{ old('plat', @$vehicle['plat']) }}">
                            @error('plat')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer p-4">
            <button type="submit" class="btn btn-sm btn-primary btn-hover-rise" id="save"><i class="fas fa-save"></i>
                Simpan</button>
            <a href="{{ route('vehicle.index') }}">
                <button type="button" class="btn btn-sm btn-danger btn-hover-rise"><i class="fas fa-times"></i>
                    Batal</button>
            </a>
        </div>
    </form>
</div>
@endsection