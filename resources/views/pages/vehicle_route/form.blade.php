@extends('templates.index')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    Form Rute Kendaraan
                </div>
            </div>
        </div>
    </div>
    <form
        action="{{ @$vehicleRoute ? route('vehicle_route.update', ['id'=>$vehicleRoute->id]) : route('vehicle_route.store')}}"
        method="POST">
        @csrf
        @if (@$vehicleRoute)
        @method('PUT')
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Kendaraan</label>
                        <div class="col-md-8">
                            <select name="vehicle_id" id="vehicle_id"
                                class="form-control @error('vehicle_id') is-invalid @enderror">
                                @foreach ($vehicle as $vehicle)
                                <option value="{{$vehicle->id}}" {{@$vehicleRoute->vehicle_id == $vehicle->id ?
                                    'selected': ''}}
                                    >{{$vehicle->name}}</option>
                                @endforeach
                            </select>
                            @error('vehicle_id')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Kelas</label>
                        <div class="col-md-8">
                            <select name="booking_class_id" id="booking_class_id"
                                class="form-control @error('booking_class_id') is-invalid @enderror">
                                @foreach ($bookingClass as $book)
                                <option value="{{$book->id}}" {{@$vehicleRoute->booking_class_id == $book->id ?
                                    'selected': ''}}
                                    >{{$book->class_name}}</option>
                                @endforeach
                            </select>
                            @error('booking_class_id')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Origin</label>
                        <div class="col-md-8">
                            <input type="text" name="origin" id="origin"
                                class="form-control @error('origin') is-invalid @enderror"
                                value="{{ old('origin', @$vehicleRoute['origin']) }}">
                            @error('origin')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Destination</label>
                        <div class="col-md-8">
                            <input type="text" name="destination" id="destination"
                                class="form-control @error('destination') is-invalid @enderror"
                                value="{{ old('destination', @$vehicleRoute['destination']) }}">
                            @error('destination')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Price</label>
                        <div class="col-md-8">
                            <input type="text" name="price" id="price"
                                class="form-control @error('price') is-invalid @enderror"
                                value="{{ old('price', @$vehicleRoute['price']) }}">
                            @error('price')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Driver name</label>
                        <div class="col-md-8">
                            <input type="text" name="driver_name" id="driver_name"
                                class="form-control @error('driver_name') is-invalid @enderror"
                                value="{{ old('driver_name', @$vehicleRoute['driver_name']) }}">
                            @error('driver_name')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Departure schedule</label>
                        <div class="col-md-8">
                            <input type="text" name="departure_schedule" id="kt_daterangepicker_1"
                                class="form-control @error('departure_schedule') is-invalid @enderror"
                                value="{{ old('departure_schedule', @$vehicleRoute['departure_schedule']) }}">
                            @error('departure_schedule')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer p-4">
            <button type="submit" class="btn btn-sm btn-primary btn-hover-rise" id="save"><i class="fas fa-save"></i>
                Simpan</button>
            <a href="{{ route('vehicle_route.index') }}">
                <button type="button" class="btn btn-sm btn-danger btn-hover-rise"><i class="fas fa-times"></i>
                    Batal</button>
            </a>
        </div>
    </form>
</div>
@endsection


@push('script')
<script>
    $("#kt_daterangepicker_1").daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        startDate: moment().startOf("year"),
        endDate: moment(),
        minDate: new Date()
    });
</script>
@endpush