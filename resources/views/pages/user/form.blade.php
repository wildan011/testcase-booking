@extends('templates.index')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    Form User
                </div>
            </div>
        </div>
    </div>
    <form action="{{ @$user ? route('user.update', ['id'=>$user->id]) : route('user.store')}}" method="POST">
        @csrf
        @if (@$user)
        @method('PUT')
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Nama</label>
                        <div class="col-md-8">
                            <input type="text" name="name" id="name"
                                class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name', @$user['name']) }}">
                            @error('name')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">No Telepon</label>
                        <div class="col-md-8">
                            <input type="text" name="phone" id="phone"
                                class="form-control @error('phone') is-invalid @enderror"
                                value="{{ old('phone', @$user['phone']) }}">
                            @error('phone')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">ALamat</label>
                        <div class="col-md-8">
                            <textarea name="address" id="address"
                                class="form-control @error('address') is-invalid @enderror"> {{ old('address', @$user['address']) }} </textarea>
                            @error('address')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Role</label>
                        <div class="col-md-8">
                            <select name="role" id="role" class="form-control @error('role') is-invalid @enderror"">
                                <option value=" admin">Admin</option>
                                <option value="direksi">Direksi</option>
                                <option value="customer">Customer</option>
                            </select>
                            @error('role')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Email</label>
                        <div class="col-md-8">
                            <input type="text" name="email" id="email"
                                class="form-control @error('email') is-invalid @enderror"
                                value="{{ old('email', @$user['email']) }}">
                            @error('email')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Password</label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password"
                                class="form-control @error('password') is-invalid @enderror"
                                value="{{ old('password', @$user['password']) }}">
                            @error('password')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer p-4">
            <button type="submit" class="btn btn-sm btn-primary btn-hover-rise" id="save"><i class="fas fa-save"></i>
                Simpan</button>
            <a href="{{ route('user.index') }}">
                <button type="button" class="btn btn-sm btn-danger btn-hover-rise"><i class="fas fa-times"></i>
                    Batal</button>
            </a>
        </div>
    </form>
</div>
@endsection