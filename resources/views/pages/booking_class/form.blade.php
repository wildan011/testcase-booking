@extends('templates.index')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    Form Tiket
                </div>
            </div>
        </div>
    </div>
    <form
        action="{{ @$bookingClass ? route('booking_class.update', ['id'=>$bookingClass->id]) : route('booking_class.store')}}"
        method="POST">
        @csrf
        @if (@$bookingClass)
        @method('PUT')
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Tipe</label>
                        <div class="col-md-8">
                            <select name="class_name" id="class_name"
                                class="form-control @error('class_name') is-invalid @enderror">
                                <option value="vip" {{@$bookingClass->class_name == "vip" ?
                                    'selected': ''}}>VIP</option>
                                <option value="bisnis" {{@$bookingClass->class_name == "bisnis" ?
                                    'selected': ''}}>Bisnis</option>
                                <option value="ekonomi" {{@$bookingClass->class_name == "ekonomi" ?
                                    'selected': ''}}>Ekonomi</option>
                            </select>
                            @error('class_name')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Price</label>
                        <div class="col-md-8">
                            <input type="text" name="price" id="price"
                                class="form-control @error('price') is-invalid @enderror"
                                value="{{ old('price', @$bookingClass['price']) }}">
                            @error('price')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer p-4">
            <button type="submit" class="btn btn-sm btn-primary btn-hover-rise" id="save"><i class="fas fa-save"></i>
                Simpan</button>
            <a href="{{ route('vehicle_route.index') }}">
                <button type="button" class="btn btn-sm btn-danger btn-hover-rise"><i class="fas fa-times"></i>
                    Batal</button>
            </a>
        </div>
    </form>
</div>
@endsection


@push('script')
<script>
    $("#kt_daterangepicker_1").daterangepicker();
</script>
@endpush