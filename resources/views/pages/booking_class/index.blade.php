@extends('templates.index')

@section('content')
<div class="card">
    <!--begin::Header-->
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">

                </div>
            </div>
        </div>
        <div class="card-toolbar">
            <a href="{{ route('booking_class.create') }}" style="margin-right: 1px" data-kt-menu-trigger="click"
                data-kt-menu-placement="bottom-end">
                <button class="btn btn-light-primary">
                    <i class="fa fa-plus-circle"></i>Tambah</button>
            </a>
        </div>
    </div>

    <div class="card-body">
        <table id="kt_booking_class_table" class="table align-middle table-row-dashed fs-6">
            <thead>
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true"
                                data-kt-check-target="#kt_booking_class_table .form-check-input" value="1" />
                        </div>
                    </th>
                    @foreach ($fieldsArray as $field)
                    @if ($field != 'id')
                    <th>{{ Str::replaceFirst('_', ' ', $field) }}</th>
                    @endif
                    @endforeach
                </tr>
            </thead>
            <tbody class="text-gray-800 fw-semibold">

            </tbody>
        </table>

    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('metronic/js/datatable.js') }}"></script>
<script>
    var columns = {!! $fieldsJson !!}
        var columnDefs = [{
                targets: 0,
                orderable: false,
                render: function(data) {
                    return `
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" value="${data}" />
                        </div>`;
                }
            },
            {
                targets: 1,
                render: function(data, type, row) {
                    return row.class_name
                }
            },
            {
                targets: 2,
                render: function(data, type, row) {
                    return "Rp. "+ new Intl.NumberFormat('en-IN').format(row.price)
                }
            },
            {
                targets: 3,
                render: function(data, type, row) {
                    return row.created_at
                }
            },
            {
                targets: -1,
                data: null,
                orderable: false,
                className: 'text-end',
                render: function(data, type, row) {
                    let editUrl = '{{ route('booking_class.edit', ['id' => ':id']) }}'
                        .replace(':id', row.id);
                    let deleteUrl = '{{ route('booking_class.destroy', ['id' => ':id']) }}'
                        .replace(':id', row.id);

                        let deleteContainer = `
                    <div class="menu-item px-3">
                        <a data-id=${deleteUrl} href="#" id="deleteItem" class="menu-link px-3" >
                            Delete
                        </a>
                    </div>
                    `

                    return `
                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="currentColor"></path>
                                </svg>
                            </span>
                        </a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">
                        <div class="menu-item px-3">
                            <a href="${editUrl}" class="menu-link px-3" data-kt-docs-table-filter="edit_row">
                                Edit
                            </a>
                        </div>
                        ${deleteContainer}`;
                },
            },
        ]


        DatatableService({
            module: "user",
            tableId: '#kt_booking_class_table',
            routes: {
                datatable: '{{ route('booking_class.datatable') }}',
            },
            columns: columns,
            columnDefs: columnDefs,
            callback: function(dt) {
                const deleteButton = document.querySelectorAll('#deleteItem')
                if (deleteButton) {
                    deleteButton.forEach(element => {
                        element.addEventListener('click', function(e) {
                            e.preventDefault();
                            var id = e.target.getAttribute('data-id');
                            Swal.fire({
                                text: "Are you sure you want to delete this?",
                                icon: "warning",
                                showCancelButton: true,
                                buttonsStyling: false,
                                confirmButtonText: "Yes, delete!",
                                cancelButtonText: "No, cancel",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-danger",
                                    cancelButton: "btn fw-bold btn-active-light-primary"
                                }
                            }).then(function(result) {
                                if (result.value) {
                                    $.ajax({
                                        url: id,
                                        data: {
                                            "_token": '{{ CSRF_TOKEN() }}'

                                        },
                                        type: "DELETE",
                                    }).done(function(response) {
                                        Swal.fire({
                                            text: "You have deleted this!.",
                                            icon: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        }).then(function() {
                                            dt.draw();
                                        });
                                    }).fail(function(xhr) {
                                        Swal.fire({
                                            text: "You dont have authorization for this action ",
                                            icon: "error",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        })
                                    });
                                }

                            });
                        })
                    })
                }
            }
        })

</script>

@endpush