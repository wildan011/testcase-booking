@extends('templates.index')

@section('content')
<div class="card">
    <!--begin::Header-->
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    <div class="w-300px">
                        <h6>cari tanggal berangkat</h6>
                    </div>
                    <input type="text" id="kt_daterangepicker_4" class="form-control">
                    <input type="hidden" id="" class="form-control start_date">
                    <input type="hidden" id="" class="form-control end_date">
                </div>
            </div>
        </div>
        <div class=" card-toolbar">

        </div>
    </div>

    <div class="card-body">
        <table id="kt_vehicle_route__table" class="table align-middle table-row-dashed fs-6">
            <thead>
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true"
                                data-kt-check-target="#kt_vehicle_route__table .form-check-input" value="1" />
                        </div>
                    </th>
                    @foreach ($fieldsArray as $field)
                    @if ($field != 'id')
                    <th>{{ Str::replaceFirst('_', ' ', $field) }}</th>
                    @endif
                    @endforeach
                </tr>
            </thead>
            <tbody class="text-gray-800 fw-semibold">

            </tbody>
        </table>

    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('metronic/js/datatable.js') }}"></script>
<script src="{{ asset('metronic/js/helper.js') }}"></script>
<script>
    filterDate()
    var columns = {!! $fieldsJson !!}
            var columnDefs = [{
                    targets: 0,
                    orderable: false,
                    render: function(data) {
                        return `
                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" value="${data}" />
                            </div>`;
                    }
                },
                {
                    targets: 1,
                    render: function(data, type, row) {
                        return row.vehicle.name
                    }
                },
                {
                    targets: 2,
                    render: function(data, type, row) {
                        return row.origin
                    }
                },
                {
                    targets: 3,
                    render: function(data, type, row) {
                        return row.destination
                    }
                },
                {
                    targets: 4,
                    render: function(data, type, row) {
                        return row.driver_name
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row) {
                        return row.booking_class.class_name
                    }
                },
                {
                    targets: 6,
                    render: function(data, type, row) {
                        return row.departure_schedule
                    }
                },
                {
                    targets: -1,
                    data: null,
                    orderable: false,
                    className: 'text-end',
                    render: function(data, type, row) {
                        let editUrl = '{{ route('departure_schedule.add', ['id' => ':id']) }}'
                            .replace(':id', row.id);

                        return `
                            <a href="` + editUrl + `" class="btn btn-sm btn-primary" data-kt-docs-table-filter="edit_row">
                                Pesan
                            </a>

                `;
                    },
                },
            ]


            DatatableService({
                module: "user",
                tableId: '#kt_vehicle_route__table',
                routes: {
                    datatable: '{{ route('vehicle_route.datatable') }}',
                },
                columns: columns,
                columnDefs: columnDefs
            })
</script>

@endpush