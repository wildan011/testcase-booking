@extends('templates.index')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex-column">
                <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0">
                    Form Pemesanan
                </div>
            </div>
        </div>
    </div>
    <form action="{{route('departure_schedule.store')}}" method="POST">
        @csrf
        @if (@$vehicleRoute)
        @method('PUT')
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group row mb-5">
                        <input type="hidden" value="{{$id}}" name="vehicle_route_id">
                        <label class="col-md-4 col-form-label form-required">Kursi</label>
                        <div class="col-md-8">
                            <select name="chair" id="chair" class="form-control @error('chair') is-invalid @enderror">
                                @for ($i = 1; $i < 10; $i++) <option value="{{$i}}">Kursi {{$i}}</option>
                                    @endfor
                            </select>
                            @error('chair')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-5">
                        <label class="col-md-4 col-form-label form-required">Transfer Via</label>
                        <div class="col-md-8">
                            <select name="transfer_via" id="transfer_via"
                                class="form-control @error('transfer_via') is-invalid @enderror">
                                <option value="bank">Bank</option>
                                <option value="ewallet">E Wallet</option>
                            </select>
                            @error('transfer_via')
                            <div class="error invalid-feedback" role="alert">
                                {{ __($message) }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer p-4">
            <button type="submit" class="btn btn-sm btn-primary btn-hover-rise" id="save"><i class="fas fa-save"></i>
                Simpan</button>
            <a href="{{ route('vehicle_route.index') }}">
                <button type="button" class="btn btn-sm btn-danger btn-hover-rise"><i class="fas fa-times"></i>
                    Batal</button>
            </a>
        </div>
    </form>
</div>
@endsection


@push('script')
<script>
    $("#kt_daterangepicker_1").daterangepicker();
</script>
@endpush