<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>
        {{ config('app.name') }} | @yield('subtitle', __('label.login'))
    </title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{ asset('vendor/metronic-3/css/pages/login/login-3.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/metronic-3/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/metronic-3/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('metronic/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    @stack('stylesheet')
    <link rel="shortcut icon" href="{{ asset('metronic/favicon.png') }}" />
    <style>
        .bg-white-o-5 {
            background: rgba(255, 255, 255, 0.02) !important
        }

        .invalid-feedback {
            display: block;
            font-weight: 500;
            font-size: 0.9rem;
        }

        .kt-login__container {
            margin-top: 25%;
            background: #FFFFFF;
            border: 1px solid #d5e0ec00;
            border-radius: 10px;
            padding: 30px;
        }

        .kt-login__logo {
            margin: 0 auto 2rem auto !important;
        }

        .kt-login__head {
            margin-bottom: 2rem !important;
        }

        .input-icons i {
            position: absolute;
        }

        .icon {
            padding: 40px 0;
            min-width: 40px;
        }

        .icon-right {
            padding: 45px 0;
            right: 30px;
            opacity: 0.5;
            min-width: 40px
        }

        .input-icons .form-control {
            /* padding: 0 30px !important; */
        }

        /*Change text in autofill textbox*/
        input:-webkit-autofill,
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus,
        input:-webkit-autofill:active {
            /* -webkit-box-shadow: 0 0 0 1000px #d5e1eb inset !important; */
            -webkit-box-shadow: 0 0 0 1000px #fff inset !important;
            -webkit-text-fill-color: black !important;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="
                background-image: url({{ asset('vendor/metronic-3/media/bg/ornament_login.png') }});
                background-color: var(--bs-primary);
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                height: 100%;">
                <div class="kt-login__wrapper">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#2c77f4",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <script src="{{ asset('vendor/metronic-3/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/metronic-3/js/scripts.bundle.js') }}" type="text/javascript"></script>
    @stack('script')
</body>

</html>