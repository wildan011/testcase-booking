@extends('auth.layout')

@section('subtitle')
Login
@endsection

@section('content')
<div class="kt-login__container d-flex shadow h-auto ">
    <div class="d-flex flex-column  justify-content-center">
        <img style="height: 376px;" class="image-login-page"
            src="{{ asset('vendor/metronic-3/media/bg/login_img.svg') }}" alt="">
    </div>
    <div class="kt-login__signin d-flex position-relative justify-content-center align-items-center flex-column ">
        <div id="form_login">
            <div class="kt-login__head">
                <h1 class="text-center mb-5 text-primary" style="font-size: 24px;
                font-style: normal;
                font-weight: 600;
                line-height: 20px;
                letter-spacing: 0.25px;;">Selamat Datang!</h1>
                <h6 class="kt-login__title">Silahkan login ke akun anda</h6>
            </div>
            <form class="kt-form" action="{{ route('login') }}" method="POST">
                @csrf
                <div class="input-group mb-4">
                    <div class="input-icon w-100">
                        <label for="" class="form-label mb-0">Email</label>
                        <input
                            class="form-control border-bottom bg-white-o-5 mt-1 @error('email') is-invalid @enderror @error('username') is-invalid @enderror"
                            type="text" placeholder="masukan email" name="email"
                            value="{{ old('email') ?: old('username') }}">
                        @error('email')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'email']) }}
                        </div>
                        @enderror
                        @error('username')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'email']) }}
                        </div>
                        @enderror
                    </div>
                </div>
                {{-- rgba(255, 255, 255, 0.02) !important --}}
                <div class="input-group">
                    <div class="input-icons w-100">
                        {{-- <i class="fa fa-lock icon"></i> --}}
                        <div class="show-hide-password">
                            <i class="fa fa-eye icon-right show-password" style="cursor: pointer"></i>
                            <i class="fa fa-eye-slash icon-right d-none hide-password"></i>
                        </div>
                        <label for="" class="form-label mb-0">Sandi</label>
                        <input class="form-control password bg-white-o-5 mt-1 @error('password') is-invalid @enderror"
                            type="password" placeholder="masukan password" name="password">
                        @error('password')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'password']) }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="form-group d-flex flex-wrap justify-content-between align-items-center kt-opacity-6 mt-3"
                    style="padding: 0 35px">
                </div>
                <div class="form-group text-center mt-5">
                </div>

                <div class="kt-login__actions">
                    <button id="kt_login_signin_submit"
                        class="btn btn-danger btn-elevate kt-login__btn-primary w-100  mb-3">Login</button>

                    <a href="{{route('register')}}">
                        <button type="button" id="kt_login_signin_submit"
                            class="btn btn-danger btn-elevate kt-login__btn-primary w-100">Daftar</button>
                    </a>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
@endsection

@push('script')
<script>
    const showPassword = document.querySelector('.show-hide-password');
    if (showPassword) {
        showPassword.addEventListener("click", function(e) {
            let password = document.querySelector(".password")
            if (password.type === "password") {
                password.type = "text"
                document.querySelector('.show-password').classList.add('d-none')
                document.querySelector('.hide-password').classList.remove('d-none')
            } else {
                password.type = "password"
                document.querySelector('.show-password').classList.remove('d-none')
                document.querySelector('.hide-password').classList.add('d-none')
            }
        })
    }
</script>

@endpush