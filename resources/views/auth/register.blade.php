@extends('auth.layout')

@section('subtitle')
Register
@endsection

@section('content')
<div class="kt-login__container d-flex shadow h-auto ">
    <div class="d-flex flex-column  justify-content-center">
        <img style="height: 300px;" class="image-login-page"
            src="{{ asset('vendor/metronic-3/media/bg/login_img.svg') }}" alt="">
    </div>
    <div class="kt-login__signin d-flex position-relative justify-content-center align-items-center flex-column ">
        <div id="form_login">
            <div class="kt-login__head">
                <h1 class="text-center mb-5 text-primary" style="font-size: 24px;
                font-style: normal;
                font-weight: 600;
                line-height: 20px;
                letter-spacing: 0.25px;;">Selamat Datang!</h1>
                <h6 class="kt-login__title">Silahkan daftarkan akun anda</h6>
            </div>
            <form class="kt-form" action="{{ route('post.register') }}" method="POST">
                @csrf
                <div class="input-group mb-4">
                    <div class="input-icon w-100">
                        <label for="" class="form-label mb-0">Nama</label>
                        <input
                            class="form-control border-bottom bg-white-o-5 mt-1 @error('name') is-invalid @enderror @error('name') is-invalid @enderror"
                            type="text" placeholder="masukan nama" name="name" value="{{ old('name') ?: old('name') }}">
                        @error('name')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'name']) }}
                        </div>
                        @error('username')
                        @enderror
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'email']) }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="input-group mb-4">
                    <div class="input-icon w-100">
                        <label for="" class="form-label mb-0">No. Telepon</label>
                        <input
                            class="form-control border-bottom bg-white-o-5 mt-1 @error('phone') is-invalid @enderror @error('phone') is-invalid @enderror"
                            type="text" placeholder="masukan telepon" name="phone"
                            value="{{ old('phone') ?: old('phone') }}">
                        @error('phone')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'phone']) }}
                        </div>
                        @enderror
                        @error('username')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'email']) }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="input-group mb-4">
                    <div class="input-icon w-100">
                        <label for="" class="form-label mb-0">Alamat</label>
                        <input
                            class="form-control border-bottom bg-white-o-5 mt-1 @error('address') is-invalid @enderror @error('address') is-invalid @enderror"
                            type="text" placeholder="masukan alamat" name="address"
                            value="{{ old('address') ?: old('address') }}">
                        @error('address')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'address']) }}
                        </div>
                        @enderror
                        @error('username')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'email']) }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="input-icon mb-4">
                    <label for="" class="form-label mb-0">Email</label>
                    <input
                        class="form-control border-bottom bg-white-o-5 mt-1 @error('email') is-invalid @enderror @error('username') is-invalid @enderror"
                        type="text" placeholder="masukan email" name="email"
                        value="{{ old('email') ?: old('username') }}">
                    @error('email')
                    <div class="error invalid-feedback" role="alert">
                        {{ __($message, ['attribute' => 'email']) }}
                    </div>
                    @enderror
                    @error('username')
                    <div class="error invalid-feedback" role="alert">
                        {{ __($message, ['attribute' => 'email']) }}
                    </div>
                    @enderror
                </div>
                <div class="input-group">
                    <div class="input-icons w-100">
                        <div class="show-hide-password">
                            <i class="fa fa-eye icon-right show-password" style="cursor: pointer"></i>
                            <i class="fa fa-eye-slash icon-right d-none hide-password"></i>
                        </div>
                        <label for="" class="form-label mb-0">Sandi</label>
                        <input class="form-control password bg-white-o-5 mt-1 @error('password') is-invalid @enderror"
                            type="password" placeholder="masukan password" name="password">
                        @error('password')
                        <div class="error invalid-feedback" role="alert">
                            {{ __($message, ['attribute' => 'password']) }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="kt-login__actions">
                    <button id="kt_login_signin_submit"
                        class="btn btn-danger btn-elevate kt-login__btn-primary w-100  mb-3">Daftar</button>
                </div>
        </div>
        </form>
    </div>
</div>
@endsection

@push('script')
<script>
    const showPassword = document.querySelector('.show-hide-password');
    if (showPassword) {
        showPassword.addEventListener("click", function(e) {
            let password = document.querySelector(".password")
            if (password.type === "password") {
                password.type = "text"
                document.querySelector('.show-password').classList.add('d-none')
                document.querySelector('.hide-password').classList.remove('d-none')
            } else {
                password.type = "password"
                document.querySelector('.show-password').classList.remove('d-none')
                document.querySelector('.hide-password').classList.add('d-none')
            }
        })
    }
</script>

@endpush