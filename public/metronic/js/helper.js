function filterDate() {
    var start = start ? start : moment().startOf("year");
    var end = moment();

    function cb(start, end) {
        $("#kt_daterangepicker_4").html(start.format("D MMMM, YYYY, HH:mm") + " - " + end.format("D MMMM, YYYY, HH:mm"));
        $(".start_date").val(start.format("YYYY-MM-DD HH:mm:00"))
        $(".end_date").val(end.format("YYYY-MM-DD HH:mm:59"))
    }

    $("#kt_daterangepicker_4").daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        startDate: start,
        endDate: end,
        minDate: new Date()
    }, cb);

    cb(start, end);

}

function downloadExcel(e) {
    e.preventDefault()
    var time = $(".filter-year option:selected").val()
    if ($('.btn-light-info.active').attr('date-value') == "M") {
        time = $(".filter-year option:selected").val() + "-" + $(".filter-month option:selected").val()
    } else if ($('.btn-light-info.active').attr('date-value') == "W") {
        time = moment($("#kt_daterangepicker_single_date").val()).format("YYYY-MM-DD")
    }
    $("input[name='regionIdProvince']").val($("#province_id").val() ? $("#province_id").val() : "all")
    $("input[name='regionIdCity']").val($("#city_id").val() ? $("#city_id").val() : "all")
    $("input[name='regionIdDistrict']").val($("#district_id").val() ? $("#district_id").val() : "all")
    $("input[name='apk']").val($("#apk_id").val() ? $("#apk_id").val() : "all")
    $("input[name='volunteer']").val($("#volunteer_id").val() ? $("#volunteer_id").val() : "all")
    $("input[name='phone']").val($("#phone").val() ? $("#phone").val() : "all")
    $("input[name='sort']").val($("#sort").val() ? $("#sort").val() : "DESC")
    $("input[name='startDate']").val($('.start_date').val())
    $("input[name='endDate']").val($('.end_date').val())
    $("input[name='timeframe']").val($(".btn-light-info.active").attr('date-value'))
    $("input[name='time']").val(time)
    document.getElementById('download-excel').submit()
}

function downloadExcelQueue(url, token) {
    var time = $(".filter-year option:selected").val()
    if ($('.btn-light-info.active').attr('date-value') == "M") {
        time = $(".filter-year option:selected").val() + "-" + $(".filter-month option:selected").val()
    } else if ($('.btn-light-info.active').attr('date-value') == "W") {
        time = moment($("#kt_daterangepicker_single_date").val()).format("YYYY-MM-DD")
    }

    if ($('.timefrime.active').attr('date-value') == "M") {
        time = $(".filter-year option:selected").val() + "-" + $(".filter-month option:selected").val()
    } else if ($('.timefrime.active').attr('date-value') == "W") {
        time = moment($("#kt_daterangepicker_single_date").val()).format("YYYY-MM-DD")
    }

    var data = {
        "page": 1,
        "size": 100,
        "regionIdProvince": $("#province_id").val() ? $("#province_id").val() : "all",
        "regionIdCity": $("#city_id").val() ? $("#city_id").val() : "all",
        "regionIdDistrict": $("#district_id").val() ? $("#district_id").val() : "all",
        "regionIdVillage": $("#village_id").val() ? $("#village_id").val() : "all",
        // "apk": $("#apk_id").val() ? $("#apk_id").val() : "all",
        // "volunteer": $("#volunteer_id").val() ? $("#volunteer_id").val() : "all",
        // "phone": $("#phone").val() ? $("#phone").val() : "all",
        // "sort": $("#sort").val() ? $("#sort").val() : "DESC",
        "startDate": $('.start_date').val(),
        "endDate": $('.end_date').val(),
        "timeframe": $(".timefrime.active").attr('date-value'),
        "time": time,
        "type": $('#type_form').val(),
        "filter_by": $('#filter_by').val() ? $('#filter_by').val() : "gender",
        "view_type": $('#view_type').val() ? $('#view_type').val() : "electability"
    }
    $.ajax({
        url: url,
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: data
    }).done(function (response) {
    })
    // document.getElementById('download-excel').submit()
}

/**
 * =========================================================================
 * This is filter for maps
 * ==========================================================================
 */
function defaultFilterMap() {
    var params = {
        "regionIdProvince": $("#province_id").val() ? $("#province_id").val() : "all",
        "regionIdCity": $("#city_id").val() ? $("#city_id").val() : "all",
        "regionIdDistrict": $("#district_id").val() ? $("#district_id").val() : "all",
        "regionIdVillage": $("#village_id").val() ? $("#village_id").val() : "all",
        "apk": $("#apk_id").val() ? $("#apk_id").val() : "all",
        "volunteer": $("#volunteer_id").val() ? $("#volunteer_id").val() : "all",
        "phone": $("#handphone").val() ? $("#handphone").val() : "all",
        "time": {
            "startDate": $('.start_date').val(),
            "endDate": $('.end_date').val(),
        },
        "startDate": $('.start_date').val(),
        "endDate": $('.end_date').val(),
        "type": $("#type_form").val() ? $("#type_form").val() : "survey"
    }
    return params;
}

function getData(urlPolygon, urlPoint, token, filename, params = {}, updateLayer = false) {
    $.get(filename, function (json) {
        if ($('#check-map-polygon').prop('checked') && $('#check-map-point').prop('checked')) {
            $('#spinner-div').show();
            mapPoint2(urlPoint, token, params, updateLayer, json)
            // mapPoint(params, updateLayer, true)
            mapPolygon(urlPolygon, token, params, updateLayer, json, true)
        } else {
            if ($('#check-map-polygon').prop('checked')) {
                mapPolygon(urlPolygon, token, params, updateLayer, json)
            }

            if ($('#check-map-point').prop('checked')) {
                // mapPoint(params, updateLayer)
                mapPoint2(urlPoint, token, params, updateLayer, json)
            }

        }
    });
}

function getData2(urlPolygon, urlPoint, token, filename, params = {}, updateLayer = false, urlAggsPoint) {
    $.get(filename, function (json) {
        // if ($('#check-map-polygon').prop('checked') && $('#check-map-point').prop('checked')) {
        $('#spinner-div').show();
        mapAggrPoint(urlAggsPoint, urlPoint, token, params, updateLayer, true)
        mapPolygon(urlPolygon, token, params, updateLayer, json, true)
        loadPercentage()
        // } else {
        //     if ($('#check-map-polygon').prop('checked')) {
        //         mapPolygon(urlPolygon, token, params, updateLayer, json)
        //     }

        //     if ($('#check-map-point').prop('checked')) {
        //         mapAggrPoint(urlAggsPoint, urlPoint, token, params, updateLayer)
        //     }

        // }
    });

}

function mapPolygon(urlPolygon, token, params = {}, updateLayer = false, json, checkedAll = false) {
    if (!checkedAll) {
        $('#spinner-div').show();
    }
    $.ajax({
        url: urlPolygon,
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': token
        },
        contentType: 'application/json',
        data: JSON.stringify(params)
    }).done(function (data) {
        var data = JSON.parse(data)
        if (data.status) {
            if (updateLayer) {
                polygonFeature = []
                pointLabelFeature = []
            }

            Object.keys(json).map((j) => {
                var defaultProperties = {
                    color: '#999',
                    region_id: 0,
                    region_name: "",
                    total_responden_report: 0,
                    total_report: 0,
                }

                let properties = data.data.data.filter((item) => {
                    return item.region_id == j
                })

                if (properties.length == 0) {
                    defaultProperties.region_id = j
                    defaultProperties.region_name = json[j].name
                }

                polygonFeature.push({
                    type: 'Feature',
                    properties: properties.length > 0 ? properties[0] :
                        defaultProperties,
                    geometry: json[j].geometry
                });

                pointLabelFeature.push({
                    type: 'Feature',
                    properties: properties.length > 0 ? properties[0] :
                        defaultProperties,
                    geometry: {
                        "type": "Point",
                        "coordinates": [json[j].longitude, json[j].latitude]
                    },
                })

                if (updateLayer) {
                    map.getSource(idLayerPolygon).setData({
                        type: "FeatureCollection",
                        features: polygonFeature
                    });

                    map.getSource(idLayerPolygonLabel).setData({
                        'type': 'FeatureCollection',
                        'features': pointLabelFeature
                    });
                }
                if ($('#check-map-polygon').prop('checked')) {

                }
            })
            if (!checkedAll) {
                $('#spinner-div').hide();
            }
        }
    })
}

function mapPoint(params = {}, updateLayer = false, checkedAll = false) {
    if (!checkedAll) {
        $('#spinner-div').show();
    }
    const socket = new SockJS('http://192.168.20.161:7200/progresio/ws');
    var stompClient = Stomp.over(socket);
    let session_id = '6832558522-5740392366-3240738938'
    pointFeature = []

    try {
        stompClient.disconnect()
    } catch (error) {
        console.log("error", error)
    }

    stompClient.connect({}, function (frame) {
        // setConnected(true);
        stompClient.subscribe('/user/' + session_id + '/topic/point', function (message) {
            var data = JSON.parse(message.body)
            $('#spinner-div').hide();
            pointFeature = pointFeature.concat(data.data);

            if (data.stop == true) {
                stompClient.disconnect()
            } else {
                if (updateLayer) {
                    map.getSource('point').setData({
                        type: "FeatureCollection",
                        features: pointFeature
                    });
                }
            }


        });
        stompClient.send("/app/point", {}, JSON.stringify({
            "regionIdProvince": params.regionIdProvince,
            "regionIdCity": params.regionIdCity,
            "regionIdDistrict": params.regionIdDistrict,
            "apk": params.apk,
            "volunteer": params.volunteer,
            "phone": params.phone,
            "startDate": params.startDate,
            "endDate": params.endDate,
            "sessionId": session_id
        }));
    });
}

function mapPoint2(urlPoint, token, params = {}, updateLayer = false, checkedAll = false) {
    if (!checkedAll) {
        $('#spinner-div').show();
    }

    $.ajax({
        url: urlPoint,
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': token
        },
        contentType: 'application/json',
        data: JSON.stringify(params)
    }).done(function (data) {
        var data = JSON.parse(data)
        if (data.status) {
            if (updateLayer) {
                pointFeature = []
            }

            data.data.data.forEach(element => {
                pointFeature.push(element);
            });

            if (updateLayer) {
                map.getSource('point').setData({
                    type: "FeatureCollection",
                    features: pointFeature
                });
            }

            $('#spinner-div').hide();
        }
    })
}

function mapAggrPoint(urlAggsPoint, urlPoint, token, params = {}, updateLayer = false, checkedAll = false) {
    if (!checkedAll) {
        $('#spinner-div').show();
    }

    let url = params.regionIdDistrict != "all" ? urlPoint : urlAggsPoint;
    let typeMap = "provinsi";
    if (params.regionIdDistrict != "all") {
        typeMap = "kelurahan"
    } else if (params.regionIdCity != "all") {
        typeMap = "kecamatan"
    } else if (params.regionIdProvince != "all") {
        typeMap = "kota"
    }

    $.ajax({
        url: url,
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': token
        },
        contentType: 'application/json',
        data: JSON.stringify(params)
    }).done(function (data) {
        var data = JSON.parse(data)
        if (data.status) {
            if (updateLayer) {
                pointFeature = []
            }

            if (params.regionIdDistrict != "all") {
                data.data.data.forEach(element => {
                    pointFeature.push(element);
                });
            } else {
                data.data.data.forEach(element => {
                    let cls = {
                        type: "Feature",
                        geometry: {
                            "type": "Point",
                            "coordinates": [
                                element.longitude,
                                element.latitude
                            ]
                        },
                        properties: {
                            ...element,
                            count: element.count,
                            value: { count: element.count },
                            label: numberFormatter(element.count),
                            countLabel: numberFormatterString(element.count, 1)
                        },
                    }
                    pointFeature.push(cls);
                });
            }

            if (updateLayer) {
                if (typeMap != "kelurahan") {
                    map.getSource('point').setData({
                        type: "FeatureCollection",
                        features: []
                    });
                    map.getSource('clusters-custom').setData({
                        type: "FeatureCollection",
                        features: pointFeature
                    });
                    if (document.getElementById('check-map-point').checked) {
                        map.setLayoutProperty('clusters', 'visibility', 'none');
                        map.setLayoutProperty('cluster-count', 'visibility', 'none');
                        map.setLayoutProperty('unclustered-point', 'visibility', 'none');

                        map.setLayoutProperty('clusters-custom-circle', 'visibility', 'visible');
                        map.setLayoutProperty('clusters-custom-label', 'visibility', 'visible');
                    }

                } else {
                    if (document.getElementById('check-map-point').checked) {
                        map.setLayoutProperty('clusters', 'visibility', 'visible');
                        map.setLayoutProperty('cluster-count', 'visibility', 'visible');
                        map.setLayoutProperty('unclustered-point', 'visibility', 'visible');

                        map.setLayoutProperty('clusters-custom-circle', 'visibility', 'none');
                        map.setLayoutProperty('clusters-custom-label', 'visibility', 'none');

                    }
                    map.getSource('clusters-custom').setData({
                        type: "FeatureCollection",
                        features: []
                    });
                    map.getSource('point').setData({
                        type: "FeatureCollection",
                        features: pointFeature
                    });
                }

                if (!document.getElementById('check-map-point').checked) {
                    map.setLayoutProperty('clusters', 'visibility', 'none');
                    map.setLayoutProperty('cluster-count', 'visibility', 'none');
                    map.setLayoutProperty('unclustered-point', 'visibility', 'none');
                    map.setLayoutProperty('clusters-custom-circle', 'visibility', 'none');
                    map.setLayoutProperty('clusters-custom-label', 'visibility', 'none');

                }

            }
            $('#spinner-div').hide();
        }
    })
}

function numberFormatterString(num, digits) {
    const lookup = [
        { value: 1, symbol: "" },
        { value: 1e3, symbol: "k" },
        { value: 1e6, symbol: "M" },
        { value: 1e9, symbol: "G" },
        { value: 1e12, symbol: "T" },
        { value: 1e15, symbol: "P" },
        { value: 1e18, symbol: "E" }
    ];
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var item = lookup.slice().reverse().find(function (item) {
        return num >= item.value;
    });
    return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
}

function numberFormatter(num) {

    return num ? num.toLocaleString('id-ID') : "0";
}


async function getCoordinateCenter(url) {
    return await fetch(url)
        .then((response) => response.json())
        .then((json) => json)
}

async function reloadMap(url, appurl, updateLayer = true) {
    const province_id = $('#province_id').val()
    const city_id = $('#city_id').val()
    const district_id = $('#district_id').val()
    var parameters = defaultFilterMap();

    if (district_id && city_id && province_id) {
        var response = await getCoordinateCenter(url + '/subdistrict?id=' + district_id)
        var pathGeojson = `${appurl}/geojson/village/${district_id}/village.json`
        var zoom = 11
    } else if (city_id && province_id) {
        var response = await getCoordinateCenter(url + '/city?id=' + city_id)
        var pathGeojson = `${appurl}/geojson/district/${city_id}/district.json`
        var zoom = 10
    } else if (province_id) {
        var response = await getCoordinateCenter(url + '/province?id=' + province_id)
        var pathGeojson = `${appurl}/geojson/city/${province_id}/city.json`
        var zoom = 9
    } else {
        var pathGeojson = `${appurl}/geojson/province/province.json`
        var zoom = 7
    }

    var longitude = response ? response.data?.[0]?.longitude : 112.733352292405140
    var latitude = response ? response.data?.[0]?.latitude : -7.720133475323289

    getData(routePolygon, routePoint, token, pathGeojson, parameters, updateLayer)
    mapFlyTo([longitude, latitude], zoom)
    removePopup()
}


async function reloadMap2(url, appurl, updateLayer = true, routeAggsPoint) {
    const province_id = $('#province_id').val()
    const city_id = $('#city_id').val()
    const district_id = $('#district_id').val()
    var parameters = defaultFilterMap();

    if (district_id && city_id && province_id) {
        var response = await getCoordinateCenter(url + '/subdistrict?id=' + district_id)
        var pathGeojson = `${appurl}/geojson/village/${district_id}/village.json`
        var zoom = 11
    } else if (city_id && province_id) {
        var response = await getCoordinateCenter(url + '/city?id=' + city_id)
        var pathGeojson = `${appurl}/geojson/district/${city_id}/district.json`
        var zoom = 10
    } else if (province_id) {
        var response = await getCoordinateCenter(url + '/province?id=' + province_id)
        var pathGeojson = `${appurl}/geojson/city/${province_id}/city.json`
        var zoom = 9
    } else {
        var pathGeojson = `${appurl}/geojson/province/province.json`
        var zoom = 7
    }

    var longitude = response ? response.data?.[0]?.longitude : 112.733352292405140
    var latitude = response ? response.data?.[0]?.latitude : -7.720133475323289

    getData2(routePolygon, routePoint, token, pathGeojson, parameters, updateLayer, routeAggsPoint)
    mapFlyTo([longitude, latitude], zoom)
    loadPercentage()
    removePopup()
}

// function for change location when filter
function mapFlyTo(coordinate = [112.733352292405140, -7.720133475323289], zoom = 7) {
    map.flyTo({
        center: coordinate,
        zoom: zoom,
        duration: 2000, // Animate over 2 seconds
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    })
}
// end function

// function for remove popup map when filtering
function removePopup() {
    if (popupPoint)
        popupPoint.remove()

    if (popupPolygon)
        popupPolygon.remove()

    if (spiderifier)
        spiderifier.unspiderfy();
}
// end function

/**
 * ==========================================================================
 * This is filter for select2
 * ==========================================================================
 */
function selectDynamic(url) {
    $('#province_id').select2({
        ajax: {
            url: `${url}/province`,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Provinsi',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    }).on('change', function () {
        if ($(this).val()) {
            $('#city_id').val(null).trigger('change').removeAttr('disabled');
        } else {
            $('#city_id').val(null).trigger('change').attr('disabled', 'disabled');
        }
        $('#district_id').val(null).trigger('change').attr('disabled', 'disabled');
    });

    $('#city_id').select2({
        ajax: {
            url: `${url}/city`,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    provinsi_id: $('#province_id').val(),
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Kota/Kabupaten',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    }).on('change', function () {
        if ($(this).val()) {
            $('#district_id').val(null).trigger('change').removeAttr('disabled');
        } else {
            $('#district_id').val(null).trigger('change').attr('disabled', 'disabled');
        }
    });

    $('#district_id').select2({
        ajax: {
            url: `${url}/subdistrict`,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    kota_kab_id: $('#city_id').val(),
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Kecamatan',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    });

    $('#volunteer_id').select2({
        ajax: {
            url: `${url}/volunteer_id/${sessionStorage.getItem('workspace_active')}`,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: {
                        keywords: params.term,
                    },
                    pagination: {
                        page: params.page ? params.page * 10 : 0,
                        perpage: params.perpage ? params.perpage : 10
                    }
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.recordsTotal
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Relawan',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    })

    $('#apk_id').select2({
        ajax: {
            url: `${url}/listapk/${sessionStorage.getItem('workspace_active')}`,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: {
                        keywords: params.term,
                    },
                    pagination: {
                        page: params.page ? params.page * 10 : 0,
                        perpage: params.perpage ? params.perpage : 10
                    }
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.recordsTotal
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih APK',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    })
}

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    var $container = $(
        "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__title'></div>" +
        "<div class='select2-result-repository__description'></div>" +
        "</div>" +
        "</div>" +
        "</div>"
    );

    $container.find(".select2-result-repository__title").text(repo.name);

    return $container;
}

function formatRepoSelection(repo) {
    return repo.name || repo.text;
}

// code for creating an SVG donut chart from feature properties
function createDonutChart(props) {
    const offsets = [];
    let counts = [];
    if(($("#type_form").val() == "post_election")) {
        counts = [
            props.mag1,
            props.mag2,
            props.mag3,
            props.mag4,
        ];
    } else {
        counts = [
            props.mag1,
            props.mag2,
            props.mag3,
        ];

    }

    let total = 0;
    for (const count of counts) {
        offsets.push(total);
        total += count;
    }
    const fontSize =
        total >= 1000 ? 22 : total >= 100 ? 20 : total >= 10 ? 18 : 16;
    const r =
        total >= 1000 ? 50 : total >= 100 ? 32 : total >= 10 ? 24 : 18;
    // const r0 = Math.round(r * 0.6);
    const r0 = Math.round(r * 0);
    const w = r * 2;

    let html = `<div>
    <svg width="${w}" height="${w}" viewbox="0 0 ${w} ${w}" text-anchor="middle" style="font: ${fontSize}px sans-serif; display: block">`;

    for (let i = 0; i < counts.length; i++) {
        html += donutSegment(
            offsets[i] / total,
            (offsets[i] + counts[i]) / total,
            r,
            r0,
            colors[i]
        );
    }
    // html += `<circle cx="${r}" cy="${r}" r="${r0}" fill="white" />
    // <text dominant-baseline="central" transform="translate(${r}, ${r})">
    //     ${total.toLocaleString()}
    // </text>
    // </svg>
    // </div>`;

        html += `<circle cx="${r}" cy="${r}" r="${r0}" fill="white" />
    <text dominant-baseline="central" transform="translate(${r}, ${r})">
    </text>
    </svg>
    </div>`;
    const el = document.createElement('div');
    el.innerHTML = html;
    return el.firstChild;
}

function donutSegment(start, end, r, r0, color) {
    if (end - start === 1) end -= 0.00001;
    const a0 = 2 * Math.PI * (start - 0.25);
    const a1 = 2 * Math.PI * (end - 0.25);
    const x0 = Math.cos(a0),
        y0 = Math.sin(a0);
    const x1 = Math.cos(a1),
        y1 = Math.sin(a1);
    const largeArc = end - start > 0.5 ? 1 : 0;

    // draw an SVG path
    return `<path d="M ${r + r0 * x0} ${r + r0 * y0} L ${r + r * x0} ${r + r * y0
        } A ${r} ${r} 0 ${largeArc} 1 ${r + r * x1} ${r + r * y1} L ${r + r0 * x1
        } ${r + r0 * y1} A ${r0} ${r0} 0 ${largeArc} 0 ${r + r0 * x0} ${r + r0 * y0
        }" fill="${color}" />`;
}
