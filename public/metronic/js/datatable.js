"use strict";

function KTDatatable(tableId, routes, columns, columnDefs, callback = null, callback_ = null) {
    // Class definition
    var KTDatatablesServerSide = function() {
        // Shared variables
        var table;
        var dt;
        var filterPayment;

        // Private functions
        var initDatatable = function() {
            dt = $(tableId).DataTable({
                searchDelay: 500,
                processing: true,
                serverSide: true,
                order: [
                    [1, 'asc']
                ],
                stateSave: false,
                // select: {
                //     style: 'multi',
                //     selector: 'td:first-child input[type="checkbox"]',
                //     className: 'row-selected'
                // },
                ajax: {
                    url: routes.datatable,
                    data: function ( d ) {
                        d.start_date = $('.start_date').val();
                        d.end_date = $('.end_date').val();
                        d.surveyor_id = $('.surveyor').val()
                        d.surveyor_id = $("#surveyor").val();
                    }
                },
                data: {},
                columns: columns,
                columnDefs: columnDefs
                // Add data-filter attribute
                // createdRow: function(row, data, dataIndex) {
                //     $(row).find('td:eq(4)').attr('data-filter', data.CreditCardType);
                // }
            });

            table = dt.$;

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            dt.on('draw', function() {
                initToggleToolbar();
                toggleToolbars();
                handleDeleteRows();
                handleDeleteUserFromWorkspace();
                if(callback) callback(dt)

                KTMenu.createInstances();
            });
        }

        var handleSearchDatatable = function() {
            const filterSearch = document.querySelector('[data-kt-docs-table-filter="search"]');
            if(filterSearch) {
                filterSearch.addEventListener('keyup', function(e) {
                    dt.search(e.target.value).draw();
                });
            }
        }

        var handleFilterDate = function() {
            $('#kt_daterangepicker_4').on('apply.daterangepicker', (e, picker) => {
                dt.ajax.reload()
            })
        }

        var handleFilterSurveyor = function() {
            const filterSearch = document.querySelector('#surveyor');
            if(filterSearch) {
                filterSearch.addEventListener('change', function(e) {
                    $('.surveyor').val(e.target.value)
                    dt.ajax.reload()
                });
            }
        }

        var handleAddQuestionOption = function() {
            const containerButtonModal = document.querySelector('[data-create="true"]')
            if (containerButtonModal) {
                containerButtonModal.addEventListener('click', function(e) {
                    e.preventDefault();
                    $('.card-form-repeater').removeClass('d-none')
                    $('.card-datatable').addClass('d-none')
                    dt.clear().draw();
                });
            }
        }

        var handleResetQuestionOption = function() {
            const containerResetButton = document.querySelector('[data-reset="true"]')
            if (containerResetButton) {
                containerResetButton.addEventListener('click', function(e) {
                    e.preventDefault();
                    document.getElementById('kt-form-question').reset()
                    $('.card-form-repeater').addClass('d-none')
                    $('.card-datatable').removeClass('d-none')
                    dt.clear().draw();
                });
            }
        }

        // var handleSearchDatatableInModal = function() {
            // const filterSearch = document.querySelector('[data-kt-docs-table-filter="searchInModal"]');
            // filterSearch.addEventListener('keyup', function(e) {
            //     $('#kt_add_workspace_users_table').DataTable().search(e.target.value).draw();
            // });
        // }

    // Init toggle toolbar
        var initToggleToolbar = function() {
            // Toggle selected action toolbar
            // Select all checkboxes
            const container = document.querySelector(tableId);
            if(container) {
                const checkboxes = container.querySelectorAll('[type="checkbox"]');
                // Select elements
                const deleteSelected = document.querySelector('[data-kt-docs-table-select="delete_selected"]');

                // Toggle delete selected toolbar
                checkboxes.forEach(c => {
                    // Checkbox on click event
                    c.addEventListener('click', function() {
                        setTimeout(function() {
                            toggleToolbars();
                        }, 50);
                    });
                });

                // Deleted selected rows
                if (deleteSelected) {
                    deleteSelected.addEventListener('click', function() {
                        // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Are you sure you want to delete selected data?",
                            icon: "warning",
                            buttonsStyling: false,
                            showCancelButton: true,
                            showLoaderOnConfirm: true,
                            confirmButtonText: "Yes, delete!",
                            cancelButtonText: "No, cancel",
                            customClass: {
                                confirmButton: "btn fw-bold btn-danger",
                                cancelButton: "btn fw-bold btn-active-light-primary"
                            },
                        }).then(function(result) {
                            if (result.value) {
                                // Simulate delete request -- for demo purpose only
                                // get selected records
                                var checkedNodes = container.querySelectorAll('[type="checkbox"]:checked');
                                var ids_ = [];
                                for (let i = 0; i < checkedNodes.length; i++) {
                                    if (checkedNodes[i] !== undefined) {
                                        if (checkedNodes[i].checked && checkedNodes[i].value != 1) {
                                            var value = checkedNodes[i].value;
                                            ids_[i] = value;
                                        }
                                    }
                                }

                                var ids = ids_.filter(function (el) {
                                    return el != null;
                                });

                                $.ajax({
                                    url: routes.destroyMany,
                                    method: 'delete',
                                    data: {
                                        _token: routes.token,
                                        id: ids
                                    }
                                }).done(function(response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Deleting selected data",
                                            icon: "info",
                                            buttonsStyling: false,
                                            showConfirmButton: false,
                                            timer: 2000
                                        }).then(function() {
                                            Swal.fire({
                                                text: "You have deleted all selected data!.",
                                                icon: "success",
                                                buttonsStyling: false,
                                                confirmButtonText: "Ok, got it!",
                                                customClass: {
                                                    confirmButton: "btn fw-bold btn-primary",
                                                }
                                            }).then(function() {
                                                // delete row data from server and re-draw datatable
                                                dt.draw();
                                            });

                                            // Remove header checked box
                                            const headerCheckbox = container
                                                .querySelectorAll(
                                                    '[type="checkbox"]')[0];
                                            headerCheckbox.checked = false;
                                        });
                                    }
                                }).fail(function(xhr){
                                    if (xhr.status == 403) {
                                        Swal.fire({
                                            text: "You dont have authorization for this action ",
                                            icon: "error",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        })
                                    }
                                })

                            } else if (result.dismiss === 'cancel') {
                                Swal.fire({
                                    text: "Selected data was not deleted.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn fw-bold btn-primary",
                                    }
                                });
                            }
                        });
                    });
                }
            }

        }

    // Filter Datatable
        var handleFilterDatatable = () => {
            // Select filter options
            // const filterButton = document.querySelector('[data-kt-user-table-filter="filter"]');
            // // // Filter datatable on submit
            // if (filterButton) {
            //     console.log(filterButton.value);
            //     $('#kt_add_workspace_users_table').DataTable().search(filterButton.value).draw();
            //     // dt.search(filterButton.value).draw();
            // //     filterButton.addEventListener('click', function(e) {
            // //         console.log(e);
            // // //         // // Get filter values
            // // //         // let paymentValue = '';

            // // //         // // Get payment value
            // // //         // filterPayment.forEach(r => {
            // // //         //     if (r.checked) {
            // // //         //         paymentValue = r.value;
            // // //         //     }

            // // //         //     // Reset payment value if "All" is selected
            // // //         //     if (paymentValue === 'all') {
            // // //         //         paymentValue = '';
            // // //         //     }
            // // //         // });

            // // //         // // Filter datatable --- official docs reference: https://datatables.net/reference/api/search()
            // //     });
            // }
        }

        // Toggle toolbars
        var toggleToolbars = function() {
            // Define variables
            const container = document.querySelector(tableId);
            // const toolbarBase = document.querySelector('[data-kt-docs-table-toolbar="base"]');
            const toolbarSelected = document.querySelector('[data-kt-docs-table-toolbar="selected"]');
            const selectedCount = document.querySelector('[data-kt-docs-table-select="delete_selected"]');

            // Select refreshed checkbox DOM elements
            const allCheckboxes = container.querySelectorAll('tbody [type="checkbox"]');

            // Detect checkboxes state & count
            let checkedState = false;
            let count = 0;

            // Count checked boxes
            allCheckboxes.forEach(c => {
                if (c.checked) {
                    checkedState = true;
                    count++;
                }
            });

            // Toggle toolbars
            if (checkedState) {
                selectedCount.innerHTML = "<i class='fa fa-trash'></i><span class='fs-7'> " + count +
                    " selected</span>";
                // toolbarBase.classList.add('d-none');
                if(toolbarSelected) {
                    toolbarSelected.classList.remove('d-none');
                }
            } else {
                // toolbarBase.classList.remove('d-none');
                if(toolbarSelected) {
                    toolbarSelected.classList.add('d-none');
                }
            }
        }

        // Delete user
        var handleDeleteRows = () => {
            // Select all delete buttons
            const deleteButtons = document.querySelectorAll('[data-kt-docs-table-filter="delete_row"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function(e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get customer name
                    const selectedId = parent.querySelectorAll('td .form-check-input')[0].value;
                    const questionIndex = parent.querySelectorAll('td .question-index')[0];
                    let customerName = parent.querySelectorAll('td')[1].innerText;
                    if (parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0] != undefined) {
                        customerName = parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0].innerHTML;
                    }

                    var route = routes.destroy.replace(':id', selectedId)
                    if(questionIndex != undefined) {
                        route = routes.destroy.replace(':id', selectedId).replace(':index', questionIndex.value)
                    }

                    Swal.fire({
                        text: "Are you sure you want to delete " + customerName + "?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Yes, delete!",
                        cancelButtonText: "No, cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            // Simulate delete request -- for demo purpose only
                            $.ajax({
                                url: route,
                                method: 'delete',
                                data: {
                                    '_token': routes.token
                                }
                            }).done(function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Deleting " +
                                            customerName,
                                        icon: "info",
                                        buttonsStyling: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    }).then(function() {
                                        Swal.fire({
                                            text: "You have deleted " +
                                                customerName +
                                                "!.",
                                            icon: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        }).then(function() {
                                            // delete row data from server and re-draw datatable
                                            dt.draw();
                                        });
                                    });
                                }
                            }).fail(function(xhr){
                                if (xhr.status == 403) {
                                    Swal.fire({
                                        text: "You dont have authorization for this action ",
                                        icon: "error",
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn fw-bold btn-primary",
                                        }
                                    })
                                }
                            })
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: customerName + " was not deleted.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Delete user from workspace
        var handleDeleteUserFromWorkspace = () => {
            // Select all delete buttons
            const deleteButtons = document.querySelectorAll('[data-kt-docs-table-filter="delete_from_workspace"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function(e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get customer name
                    const selectedId = parent.querySelectorAll('td .form-check-input')[0].value;
                    const questionIndex = parent.querySelectorAll('td .question-index')[0];
                    // const customerName = parent.querySelectorAll('td')[1].innerText;
                    const customerName = parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0].innerHTML;

                    var route = routes.removeUser.replace(':id', selectedId)
                    if(questionIndex != undefined) {
                        route = routes.removeUser.replace(':id', selectedId).replace(':index', questionIndex.value)
                    }

                    Swal.fire({
                        text: "Are you sure you want to delete " + customerName + " from this workspace ?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Yes, delete!",
                        cancelButtonText: "No, cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            // Simulate delete request -- for demo purpose only
                            $.ajax({
                                url: route,
                                method: 'delete',
                                data: {
                                    '_token': routes.token
                                }
                            }).done(function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Deleting " +
                                            customerName,
                                        icon: "info",
                                        buttonsStyling: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    }).then(function() {
                                        Swal.fire({
                                            text: "You have deleted " +
                                                customerName +
                                                "!.",
                                            icon: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        }).then(function() {
                                            // delete row data from server and re-draw datatable
                                            dt.draw();
                                        });
                                    });
                                }
                            }).fail(function(xhr){
                                if (xhr.status == 403) {
                                    Swal.fire({
                                        text: "You dont have authorization for this action ",
                                        icon: "error",
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn fw-bold btn-primary",
                                        }
                                    })
                                }
                            })
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: customerName + " was not deleted.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Public methods
        return {
            init: function() {
                initDatatable();
                handleSearchDatatable();
                initToggleToolbar();
                handleFilterDatatable();
                handleDeleteRows();
                handleDeleteUserFromWorkspace();
                handleFilterDate();
                handleFilterSurveyor();
                handleAddQuestionOption();
                handleResetQuestionOption();
                if(callback) callback(dt);
                if(callback_) callback_(dt);
            }
        }
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function() {
        KTDatatablesServerSide.init();
    });
}

function DatatableService({
    module,
    tableId,
    routes,
    sessions,
    columns,
    columnDefs,
    callback=null
}) {

    var KTDatatablesServerSideService = function() {
        // Shared variables
        var dt;
        var header = {
            "Authorization": sessionStorage.getItem('access_token')
        }
        // Private functions
        var initDatatable = function() {
            dt = $(tableId).DataTable({
                pagingType: module == 'datalapor' ? "simple" : "full_numbers",
                language: module == 'datalapor' 
                            ? {
                                paginate: {
                                    previous: '< Sebelumnya',
                                    next: 'Selanjutnya >'
                                }
                            } 
                            : '',
                searchDelay: 500,
                processing: true,
                serverSide: true,
                order: [
                    [1, 'asc']
                ],
                stateSave: false,
                ajax: {
                    url: routes.datatable,
                    headers: module=="user" ? header: {},
                    method: 'GET',
                    data: function(d) {
                        var workspace_id = "";
                        var role_id = ""
                        var organisasi_id = ""

                        if(module == "user") {
                            if(sessionStorage.getItem('workspace_system') != 1) {
                                workspace_id = sessionStorage.getItem('workspace_system')
                            }

                            if(sessionStorage.getItem('workspace_active')) {
                                workspace_id = sessionStorage.getItem('workspace_active') != "all" ? sessionStorage.getItem('workspace_active') : ""
                            }

                            if (sessionStorage.getItem('role_id')) {
                                role_id = sessionStorage.getItem('role_id') != "all" ? sessionStorage.getItem('role_id') : ""
                            }

                            if (sessionStorage.getItem('organisasi_id')) {
                                organisasi_id = sessionStorage.getItem('organisasi_id') != "all" ? sessionStorage.getItem('organisasi_id') : ""
                            }
                        }

                        if (module == "relawan" || module == "pemilih" || module == "datalapor") {
                            d.regionIdProvince = $("#province_id").val();
                            d.regionIdCity = $("#city_id").val();
                            d.regionIdDistrict = $("#district_id").val();
                            d.apk = $("#apk_id").val();
                            d.volunteer = $("#volunteer_id").val();
                            d.phone = $("#phone").val();
                            d.sort = $("#sort").val();
                        }

                        if (module == "organization") {
                            d.regionIdProvince = $("#province_id").val();
                            d.regionIdCity = $("#city_id").val();
                            d.regionIdDistrict = $("#district_id").val();
                            d.apk = $("#apk_id").val();
                            d.sort = $("#sort").val();
                            d.startDate = $('.start_date').val();
                            d.endDate = $('.end_date').val();
                            d.page = d.start
                            d.size = d.length
                        }
                        d.start_date = $('.start_date').val();
                        d.end_date = $('.end_date').val();
                        d.surveyor_id = $('.surveyor').val();
                        d.query = {
                            role_id,
                            workspace_id,
                            organisasi_id,
                            is_active: sessionStorage.getItem('status'),
                            keywords: d.search.value
                        }
                        d.pagination =  {
                            page: d.start,
                            perpage: d.length
                        }
                        d.language = 'id';
                        d.keywords = d.search.value
                        d.keyword = d.search.value
                        d.page = d.start
                        d.perpage = d.length
                        d.type = $('#type_form').val();
                        // d.name = "all",
                        // d.startDate= "2023-09-01 00:00:00 +0700",
                        // d.endDate= "2023-09-30 23:59:59 +0700",
                        // d.page= 1,
                        // d.size= 10
                    }
                },
                data:{},
                columns: columns,
                columnDefs: columnDefs
                // Add data-filter attribute
                // createdRow: function(row, data, dataIndex) {
                //     $(row).find('td:eq(4)').attr('data-filter', data.CreditCardType);
                // }
            });

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            dt.on('draw', function() {
                activeDeactiveData()
                handleDeleteUserFromWorkspace();
                initToggleToolbar();
                toggleToolbars();
                handleDeleteRows();
                handleFilterSurveyor()
                if (callback) callback(dt)

                KTMenu.createInstances();
            });
        }

        // active deactive data
        var activeDeactiveData = () => {
            const activedeactive = document.querySelectorAll('.active_deactive_user')
            if (activedeactive) {
                activedeactive.forEach(element => {
                    element.addEventListener("click", (function(t) {
                        t.preventDefault()
                        var isActive = t.target.getAttribute('data-activated')
                        var statusActive = 0
                        var message = 'Are you sure want deactive this data ?'
                        if (isActive == false) {
                            message = 'Are you sure want active this data ?'
                            statusActive = 1
                        }

                        Swal.fire({
                            text: message,
                            icon: "warning",
                            showCancelButton: true,
                            buttonsStyling: false,
                            confirmButtonText: "Yes!",
                            cancelButtonText: "No",
                            customClass: {
                                confirmButton: "btn fw-bold btn-danger",
                                cancelButton: "btn fw-bold btn-active-light-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {
                                $.ajax({
                                    url: routes.activeDeactive+'/'+t.target.getAttribute('data-ids'),
                                    method: 'PUT',
                                    headers: {
                                        "Authorization": sessionStorage.getItem('access_token')
                                    },
                                    data: {
                                        is_active: statusActive
                                    }
                                }).done(function(response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "proccessing",
                                            icon: "info",
                                            buttonsStyling: false,
                                            showConfirmButton: false,
                                            timer: 2000
                                        }).then(function() {
                                            Swal.fire({
                                                text: "Successfully update data",
                                                icon: "success",
                                                buttonsStyling: false,
                                                confirmButtonText: "Ok, got it!",
                                                customClass: {
                                                    confirmButton: "btn fw-bold btn-primary",
                                                }
                                            }).then(function() {
                                                dt.draw()
                                            });

                                        });
                                    }
                                })
                            }
                        })
                    }))
                });
            }
        }

        var refreshUserOauthSession = () => {
            $.ajax({
                url: 'http://localhost:2222/v1/app/user/profile',
                method: 'get',
                headers: {
                    "Authorization": sessionStorage.getItem('access_token')
                }
            }).done(function(response) {
                if(response.success) {
                    sessionStorage.setItem("user_oauth", response.data);
                }
            })
        }

        // Delete user from workspace
        var handleDeleteUserFromWorkspace = () => {
            // Select all delete buttons
            const deleteButtons = document.querySelectorAll('[data-kt-docs-table-filter="delete_from_workspace"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function(e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get customer name
                    const selectedId = parent.querySelectorAll('td .form-check-input')[0].value;
                    const questionIndex = parent.querySelectorAll('td .question-index')[0];
                    // const customerName = parent.querySelectorAll('td')[1].innerText;
                    const customerName = parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0].innerHTML;

                    var route = routes.removeUserFromWorkspace+'/'+selectedId
                    if(questionIndex != undefined) {
                        route = routes.removeUserFromWorkspace+'/'+selectedId+'/'+questionIndex.value
                    }

                    Swal.fire({
                        text: "Are you sure you want to delete " + customerName + " from this workspace ?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Yes, delete!",
                        cancelButtonText: "No, cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            $.ajax({
                                url: route,
                                method: 'delete',
                                headers: {
                                    "Authorization": sessionStorage.getItem('access_token')
                                },
                                data: {
                                    is_delete_workspace_user: true,
                                    workspace_id: sessionStorage.getItem('workspace_active')
                                }
                            }).done(function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Deleting " +
                                            customerName,
                                        icon: "info",
                                        buttonsStyling: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    }).then(function() {
                                        Swal.fire({
                                            text: "You have deleted " +
                                                customerName +
                                                "!.",
                                            icon: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        }).then(function() {
                                            // delete row data from server and re-draw datatable
                                            refreshUserOauthSession()
                                            dt.draw();
                                        });
                                    });
                                }
                            }).fail(function(xhr){
                                if (xhr.status == 403) {
                                    Swal.fire({
                                        text: "You dont have authorization for this action ",
                                        icon: "error",
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn fw-bold btn-primary",
                                        }
                                    })
                                }
                            })
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: customerName + " was not deleted.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Init toggle toolbar
        var initToggleToolbar = function() {
            // Toggle selected action toolbar
            // Select all checkboxes
            const container = document.querySelector(tableId);
            if(container) {
                const checkboxes = container.querySelectorAll('[type="checkbox"]');
                // Select elements
                const deleteSelected = document.querySelector('[data-kt-docs-table-select="delete_selected"]');

                // Toggle delete selected toolbar
                checkboxes.forEach(c => {
                    // Checkbox on click event
                    c.addEventListener('click', function() {
                        setTimeout(function() {
                            toggleToolbars();
                        }, 50);
                    });
                });

                // Deleted selected rows
                if (deleteSelected) {
                    deleteSelected.addEventListener('click', function() {
                        // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Are you sure you want to delete selected data?",
                            icon: "warning",
                            buttonsStyling: false,
                            showCancelButton: true,
                            showLoaderOnConfirm: true,
                            confirmButtonText: "Yes, delete!",
                            cancelButtonText: "No, cancel",
                            customClass: {
                                confirmButton: "btn fw-bold btn-danger",
                                cancelButton: "btn fw-bold btn-active-light-primary"
                            },
                        }).then(function(result) {
                            if (result.value) {
                                // Simulate delete request -- for demo purpose only
                                // get selected records
                                var checkedNodes = container.querySelectorAll('[type="checkbox"]:checked');
                                var ids_ = [];
                                for (let i = 0; i < checkedNodes.length; i++) {
                                    if (checkedNodes[i] !== undefined) {
                                        if (checkedNodes[i].checked && checkedNodes[i].value != 1) {
                                            var value = checkedNodes[i].value;
                                            ids_[i] = value;
                                        }
                                    }
                                }

                                var ids = ids_.filter(function (el) {
                                    return el != null;
                                });

                                $.ajax({
                                    url: routes.destroyMany,
                                    method: 'delete',
                                    headers: {
                                        "Authorization": sessionStorage.getItem('access_token')
                                    },
                                    data: {
                                        is_delete_workspace_user: sessionStorage.getItem('workspace_system') ? null : true,
                                        workspace_id: sessionStorage.getItem('workspace_active'),
                                        id: ids
                                    }
                                }).done(function(response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Deleting selected data",
                                            icon: "info",
                                            buttonsStyling: false,
                                            showConfirmButton: false,
                                            timer: 2000
                                        }).then(function() {
                                            Swal.fire({
                                                text: "You have deleted all selected data!.",
                                                icon: "success",
                                                buttonsStyling: false,
                                                confirmButtonText: "Ok, got it!",
                                                customClass: {
                                                    confirmButton: "btn fw-bold btn-primary",
                                                }
                                            }).then(function() {
                                                // delete row data from server and re-draw datatable
                                                dt.draw();
                                            });

                                            // Remove header checked box
                                            const headerCheckbox = container
                                                .querySelectorAll(
                                                    '[type="checkbox"]')[0];
                                            headerCheckbox.checked = false;
                                        });
                                    }
                                }).fail(function(xhr){
                                    if (xhr.status == 403) {
                                        Swal.fire({
                                            text: "You dont have authorization for this action ",
                                            icon: "error",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        })
                                    }
                                })

                            } else if (result.dismiss === 'cancel') {
                                Swal.fire({
                                    text: "Selected data was not deleted.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn fw-bold btn-primary",
                                    }
                                });
                            }
                        });
                    });
                }
            }
        }

        // Toggle toolbars
        var toggleToolbars = function() {
            // Define variables
            const container = document.querySelector(tableId);
            // const toolbarBase = document.querySelector('[data-kt-docs-table-toolbar="base"]');
            const toolbarSelected = document.querySelector('[data-kt-docs-table-toolbar="selected"]');
            const selectedCount = document.querySelector('[data-kt-docs-table-select="delete_selected"]');

            // Select refreshed checkbox DOM elements
            const allCheckboxes = container.querySelectorAll('tbody [type="checkbox"]');

            // Detect checkboxes state & count
            let checkedState = false;
            let count = 0;

            // Count checked boxes
            allCheckboxes.forEach(c => {
                if (c.checked) {
                    checkedState = true;
                    count++;
                }
            });

            // Toggle toolbars
            if (checkedState) {
                selectedCount.innerHTML = "<i class='fa fa-trash'></i><span class='fs-7'> " + count +
                    " selected</span>";
                // toolbarBase.classList.add('d-none');
                if(toolbarSelected) {
                    toolbarSelected.classList.remove('d-none');
                }
            } else {
                // toolbarBase.classList.remove('d-none');
                if(toolbarSelected) {
                    toolbarSelected.classList.add('d-none');
                }
            }
        }

        // Delete user
        var handleDeleteRows = () => {
            // Select all delete buttons
            const deleteButtons = document.querySelectorAll('[data-kt-docs-table-filter="delete_row"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function(e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get customer name
                    const selectedId = parent.querySelectorAll('td .form-check-input')[0].value;
                    const questionIndex = parent.querySelectorAll('td .question-index')[0];
                    let customerName = parent.querySelectorAll('td')[1].innerText;
                    if (parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0] != undefined) {
                        customerName = parent.querySelectorAll('td')[1].getElementsByClassName('text-email')[0].innerHTML;
                    }

                    var route = routes.deleteUser+'/'+selectedId
                    if(questionIndex != undefined) {
                        route = routes.deleteUser+'/'+selectedId+'/'+questionIndex.value
                    }

                    Swal.fire({
                        text: "Are you sure you want to delete " + customerName + "?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Yes, delete!",
                        cancelButtonText: "No, cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            // Simulate delete request -- for demo purpose only
                            $.ajax({
                                url: route,
                                method: 'delete',
                                headers: {
                                    "Authorization": sessionStorage.getItem('access_token')
                                },
                            }).done(function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Deleting " +
                                            customerName,
                                        icon: "info",
                                        buttonsStyling: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    }).then(function() {
                                        Swal.fire({
                                            text: "You have deleted " +
                                                customerName +
                                                "!.",
                                            icon: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: {
                                                confirmButton: "btn fw-bold btn-primary",
                                            }
                                        }).then(function() {
                                            // delete row data from server and re-draw datatable
                                            dt.draw();
                                        });
                                    });
                                }
                            }).fail(function(xhr){
                                if (xhr.status == 403) {
                                    Swal.fire({
                                        text: "You dont have authorization for this action ",
                                        icon: "error",
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn fw-bold btn-primary",
                                        }
                                    })
                                }
                            })
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: customerName + " was not deleted.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        var handleSearchDatatable = function() {
            const filterSearch = document.querySelector('[data-kt-docs-table-filter="search"]');
            if(filterSearch) {
                filterSearch.addEventListener('keyup', function(e) {
                    if(e.target.value.length > 2 || e.target.value.length == 0) {
                        dt.search(e.target.value).draw();
                    }
                });
            }
        }

        var handleFilterDate = function() {
            $('#kt_daterangepicker_4').on('apply.daterangepicker', (e, picker) => {
                dt.ajax.reload()
            })
        }

        var handleFilterSurveyor = function() {
            const filterSearch = document.querySelector('#surveyor');
            if(filterSearch) {
                filterSearch.addEventListener('change', function(e) {
                    $('.surveyor').val(e.target.value)
                    dt.ajax.reload()
                });
            }
        }

        // Public methods
        return {
            init: function() {
                initDatatable();
                handleSearchDatatable();
                handleFilterDate();
            }
        }
    }();
    // On document ready
    KTUtil.onDOMContentLoaded(function() {
        KTDatatablesServerSideService.init();
    });
}
