function filterDate() {
    var start = start ? start : moment().startOf("weeks");
    var end = moment();

    function cb(start, end) {
        $("#kt_daterangepicker_4").html(start.format("D MMMM, YYYY") + " - " + end.format("D MMMM, YYYY"));
        $(".start_date").val(start.format("YYYY-MM-DD 00:00:00 ZZ"))
        $(".end_date").val(end.format("YYYY-MM-DD 23:59:59 ZZ"))
    }

    $("#kt_daterangepicker_4").daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            // "Today": [moment(), moment()],
            // "This Week": [moment().subtract(1, "weeks"), moment().subtract(1, 'weeks').endOf('week')],
            // "Last 7 Days": [moment().subtract(7, "days"), moment()],
            // "Last 30 Days": [moment().subtract(30, "days"), moment()],
            "Minggu ini": [moment().startOf("weeks"), moment().endOf("weeks")],
            "Bulan ini": [moment().startOf("month"), moment().endOf("month")],
            "Tahun ini": [moment().startOf("year"), moment().endOf("year")],
            // "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf(
            //     "month")]
        },
        locale: {
            customRangeLabel: "Custom tanggal"
        },
        maxDate: new Date()
    }, cb);

    cb(start, end);

}

function downloadExcel(e) {
    e.preventDefault()
    $("input[name='regionIdProvince']").val($("#province_id").val() ? $("#province_id").val() : "all")
    $("input[name='regionIdCity']").val($("#city_id").val() ? $("#city_id").val() : "all")
    $("input[name='regionIdDistrict']").val($("#district_id").val() ? $("#district_id").val() : "all")
    $("input[name='apk']").val($("#apk_id").val() ? $("#apk_id").val() : "all")
    $("input[name='volunteer']").val($("#volunteer_id").val() ? $("#volunteer_id").val() : "all")
    $("input[name='phone']").val($("#phone").val() ? $("#phone").val() : "all")
    $("input[name='sort']").val($("#sort").val() ? $("#sort").val() : "DESC")
    $("input[name='startDate']").val($('.start_date').val())
    $("input[name='endDate']").val($('.end_date').val())
    document.getElementById('download-excel').submit()
}

function downloadExcelQueue(url, token) {
    var data = {
        "page": 1,
        "size": 100,
        "regionIdProvince": $("#province_id").val() ? $("#province_id").val() : "all",
        "regionIdCity": $("#city_id").val() ? $("#city_id").val() : "all",
        "regionIdDistrict": $("#district_id").val() ? $("#district_id").val() : "all",
        "regionIdVillage": $("#village_id").val() ? $("#village_id").val() : "all",
        "apk": $("#apk_id").val() ? $("#apk_id").val() : "all",
        "volunteer": $("#volunteer_id").val() ? $("#volunteer_id").val() : "all",
        "phone": $("#phone").val() ? $("#phone").val() : "all",
        "sort": $("#sort").val() ? $("#sort").val() : "DESC",
        "startDate": $('.start_date').val(),
        "endDate": $('.end_date').val()
    }
    $.ajax({
        url: url,
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: data
    }).done(function (response) {
        console.log(response);
    })
    // document.getElementById('download-excel').submit()
}

/**
 * =========================================================================
 * This is filter for maps
 * ==========================================================================
 */
function defaultFilterMap() {
    var params = {
        "regionIdProvince": $("#province_id").val() ? $("#province_id").val() : "all",
        "regionIdCity": $("#city_id").val() ? $("#city_id").val() : "all",
        "regionIdDistrict": $("#district_id").val() ? $("#district_id").val() : "all",
        "apk": $("#apk_id").val() ? $("#apk_id").val() : "all",
        "volunteer": $("#volunteer_id").val() ? $("#volunteer_id").val() : "all",
        "phone": $("#handphone").val() ? $("#handphone").val() : "all",
        "startDate": $('.start_date').val(),
        "endDate": $('.end_date').val()
    }
    return params;
}

function getData(urlPolygon, urlPoint, token, filename, params = {}, updateLayer = false) {
    $.get(filename, function(json) {
        $.ajax({
            url: urlPolygon,
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': token
            },
            contentType: 'application/json',
            data: JSON.stringify(params)
        }).done(function(data) {
            var data = JSON.parse(data)
            if (data.status) {
                if (updateLayer) {
                    polygonFeature = []
                    pointLabelFeature = []
                }

                Object.keys(json).map((j) => {

                    var defaultProperties = {
                        color: '#E1E7E7',
                        region_id: 0,
                        region_name: "",
                        total_apk: [],
                        total_report: 0,
                        total_responden: 0,
                        total_volunteer_report: 0
                    }


                    let properties = data.data.filter((item) => {
                        return item.region_id == j
                    })

                    if (properties.length == 0) {
                        defaultProperties.region_id = j
                        defaultProperties.region_name = json[j].name
                    }


                    polygonFeature.push({
                        type: 'Feature',
                        properties: properties.length > 0 ? properties[0] :
                            defaultProperties,
                        geometry: json[j].geometry
                    });

                    pointLabelFeature.push({
                        type: 'Feature',
                        properties: properties.length > 0 ? properties[0] :
                            defaultProperties,
                        geometry: {
                            "type": "Point",
                            "coordinates": [json[j].longitude, json[j].latitude]
                        },
                    })

                    if (updateLayer) {
                        map.getSource(idLayerPolygon).setData({
                            type: "FeatureCollection",
                            features: polygonFeature
                        });

                        map.getSource(idLayerPolygonLabel).setData({
                            'type': 'FeatureCollection',
                            'features': pointLabelFeature
                        });
                    }
                })

            }
        })

        $.ajax({
            url: urlPoint,
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': token
            },
            contentType: 'application/json',
            data: JSON.stringify(params)
        }).done(function(data) {
            var data = JSON.parse(data)
            if (data.status) {
                if (updateLayer) {
                    pointFeature = []
                }

                data.data.forEach(element => {
                    pointFeature.push(element);
                });

                if (updateLayer) {
                    map.getSource('point').setData({
                        type: "FeatureCollection",
                        features: pointFeature
                    });;
                }
            }
        })
    });
}

async function getCoordinateCenter(url) {
    return await fetch(url)
        .then((response) => response.json())
        .then((json) => json)
}

async function reloadMap(url, appurl, updateLayer=true) {
    const province_id = $('#province_id').val()
    const city_id = $('#city_id').val()
    const district_id = $('#district_id').val()
    var parameters = defaultFilterMap();

    if (district_id && city_id && province_id) {
        var response = await getCoordinateCenter(url+'/subdistrict?id=' + district_id)
        var pathGeojson = `${appurl}/geojson/village/${district_id}/village.json`
        var zoom = 11
    } else if (city_id && province_id) {
        var response = await getCoordinateCenter(url+'/city?id=' + city_id)
        var pathGeojson = `${appurl}/geojson/district/${city_id}/district.json`
        var zoom = 10
    } else if (province_id) {
        var response = await getCoordinateCenter(url+'/province?id=' + province_id)
        var pathGeojson = `${appurl}/geojson/city/${province_id}/city.json`
        var zoom = 9
    } else {
        var pathGeojson = `${appurl}/geojson/province/province.json`
        var zoom = 7
    }

    var longitude = response ? response.data?.[0]?.longitude : 106.816666
    var latitude = response ? response.data?.[0]?.latitude : -6.200000

    getData(routePolygon, routePoint, token, pathGeojson, parameters, updateLayer)
    mapFlyTo([longitude, latitude], zoom)
    removePopup()
}

// function for change location when filter
function mapFlyTo(coordinate = [106.816666, -6.200000], zoom = 7) {
    map.flyTo({
        center: coordinate,
        zoom: zoom,
        duration: 2000, // Animate over 2 seconds
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    })
}
// end function

// function for remove popup map when filtering
function removePopup() {
    if (popupPoint)
        popupPoint.remove()

    if (popupPolygon)
        popupPolygon.remove()

    if(spiderifier)
        spiderifier.unspiderfy();
}
// end function

/**
 * ==========================================================================
 * This is filter for select2
 * ==========================================================================
 */
function selectDynamic(url) {
    $('#province_id').select2({
        ajax: {
            url: `${url}/province`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Provinsi',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    }).on('change', function() {
        if ($(this).val()) {
            $('#city_id').val(null).trigger('change').removeAttr('disabled');
        } else {
            $('#city_id').val(null).trigger('change').attr('disabled', 'disabled');
        }
        $('#district_id').val(null).trigger('change').attr('disabled', 'disabled');
    });

    $('#city_id').select2({
        ajax: {
            url: `${url}/city`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    provinsi_id: $('#province_id').val(),
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Kota/Kabupaten',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    }).on('change', function() {
        if ($(this).val()) {
            $('#district_id').val(null).trigger('change').removeAttr('disabled');
        } else {
            $('#district_id').val(null).trigger('change').attr('disabled', 'disabled');
        }
    });

    $('#district_id').select2({
        ajax: {
            url: `${url}/subdistrict`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    kota_kab_id: $('#city_id').val(),
                    keywords: params.term,
                    page: params.page ? params.page * 10 : 0,
                    perpage: params.perpage ? params.perpage : 10
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.meta.total
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Kecamatan',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    });

    $('#volunteer_id').select2({
        ajax: {
            url: `${url}/volunteer_id/${sessionStorage.getItem('workspace_active')}`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    query: {
                        keywords: params.term,
                    },
                    pagination: {
                        page: params.page ? params.page * 10 : 0,
                        perpage: params.perpage ? params.perpage : 10
                    }
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.recordsTotal
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih Relawan',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    })

    $('#apk_id').select2({
        ajax: {
            url: `${url}/listapk/${sessionStorage.getItem('workspace_active')}`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    query: {
                        keywords: params.term,
                    },
                    pagination: {
                        page: params.page ? params.page * 10 : 0,
                        perpage: params.perpage ? params.perpage : 10
                    }
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                params.perpage = params.perpage || 10;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 10) < data.recordsTotal
                    }
                };
            },
            cache: true,
        },

        placeholder: 'Pilih APK',
        templateResult: formatRepo,
        // maximumSelectionLength: 3,
        allowClear: true,
        templateSelection: formatRepoSelection
    })
}

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    var $container = $(
        "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__title'></div>" +
        "<div class='select2-result-repository__description'></div>" +
        "</div>" +
        "</div>" +
        "</div>"
    );

    $container.find(".select2-result-repository__title").text(repo.name);

    return $container;
}

function formatRepoSelection(repo) {
    return repo.name || repo.text;
}
