<?php

namespace App\Models;

use App\Traits\Datatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleRoute extends Model
{
    use HasFactory, Datatable;

    protected $fillable = [
        'vehicle_id',
        'booking_class_id',
        'origin',
        'destination',
        'departure_schedule',
        'departure_schedule_start',
        'departure_schedule_end',
        'driver_name',
        'price'
    ];

    function vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }

    function bookingClass()
    {
        return $this->belongsTo(BookingClass::class, 'booking_class_id');
    }

    public static function scopeFilterDatatable($query)
    {
        $params = request();
        $start_date = !empty($params['start_date']) ? Carbon::parse($params['start_date'])->format('Y-m-d')  : null;
        $end_date = !empty($params['end_date']) ? Carbon::parse($params['end_date'])->format('Y-m-d') : null;

        return $query->when($start_date, function ($query) use ($start_date, $end_date) {
            return $query->where('departure_schedule_start', '>=', $start_date)
                ->where('departure_schedule_start', '<=', $end_date);
        });
    }
}
