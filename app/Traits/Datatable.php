<?php

namespace App\Traits;

trait Datatable
{

    public static function scopeDatatable($query, $formatter = null)
    {
        $keyword = request('keywords') ?? null;
        $sort = [
            [
                'field' => static::getDefaultSortField(),
                'sort' => static::getDefaultSort()
            ]
        ];

        $page = request()->get('page', 1);
        $perpage = request()->get('perpage', 10);

        // request()->merge(['page' => $page]);

        request()->merge(['page' => ($page / $perpage) + 1]);

        $paginate = $query->filterDatatable()
            ->searchDatatable($keyword)
            ->orderByDatatable($sort)
            ->paginate($perpage);
        return [
            'success' => true,
            'message' => 'get list successfully',
            'data' => is_callable($formatter) ? $formatter($paginate->items()) : (class_exists($formatter) ? new $formatter($paginate->items()) : static::formatter($paginate->items())),
            'meta' => [
                'page' => (int) $paginate->currentPage(),
                'pages' => (int) ceil($paginate->total() / $paginate->perPage()),
                'perpage' => (int) $paginate->perPage(),
                'total' => (int) $paginate->total(),
                'sort' => $sort[0]['sort'],
                'field' => $sort[0]['field'],
            ],
            "recordsTotal" => (int) $paginate->total(),
            "recordsFiltered" => (int) $paginate->total(),
        ];
    }

    public static function scopeFilterDatatable($query)
    {
        return $query;
    }

    public static function scopeSearchDatatable($query, $keyword)
    {
        $searchables = static::getSearchableColumns();
        return $query->when($keyword, function ($query) use ($keyword, $searchables) {
            if ($searchables) {
                $query->where(function ($query) use ($keyword, $searchables) {
                    function getGrouppedSearchable($field)
                    {
                        $splitted = explode('.', $field);
                        if (count($splitted) > 1) {
                            $key = array_shift($splitted);
                            return [
                                $key => count($splitted) > 1 ? getGrouppedSearchable(implode('.', $splitted)) : [array_shift($splitted)]
                            ];
                        } else {
                            return [array_shift($splitted)];
                        }
                    }

                    $grouppedSearchables = [];
                    foreach ($searchables as $field) {
                        $grouppedSearchables = array_merge_recursive($grouppedSearchables, getGrouppedSearchable($field));
                    }

                    function buildGrouppedSearchablesQuery($query, $keyword, $relationship, $fields)
                    {
                        if (is_array($fields)) {
                            $query->orWhereHas($relationship, function ($query) use ($fields, $keyword) {
                                $query->where(function ($query) use ($keyword, $fields) {
                                    foreach ($fields as $key => $field) {
                                        buildGrouppedSearchablesQuery($query, $keyword, $key, $field);
                                    }
                                });
                            });
                        } else {
                            $query->orWhere($fields, 'like', "%{$keyword}%");
                        }
                    }

                    foreach ($grouppedSearchables as $relationship => $fields) {
                        buildGrouppedSearchablesQuery($query, $keyword, $relationship, $fields);
                    }
                });
            }
        });
    }

    public static function scopeOrderByDatatable($query, $sorts)
    {
        foreach ($sorts as $sort) {
            if ($sort['field']) {
                $query->orderBy($sort['field'], $sort['sort']);
            }
        }
        return $query;
    }

    public static function getSearchableColumns()
    {
        return [];
    }

    public static function getDefaultSortField()
    {
        return "updated_at";
    }

    public static function getDefaultSort()
    {
        return 'desc';
    }

    public static function formatter($collections)
    {
        return $collections;
    }
}
