<?php

namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    function index()
    {
        $fields = ['id', 'vehicle', 'origin', 'destination', 'price', 'driver', 'Type Class', 'departure schedule', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.vehicle_route.index", compact('fieldsArray', 'fieldsJson'));
    }
}
