<?php

namespace App\Http\Controllers\BookingClass;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use App\Models\BookingClass;
use Illuminate\Http\Request;

class BookingClassController extends Controller
{
    function index()
    {
        $fields = ['id', 'vehicle', 'price', 'created_at', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.booking_class.index", compact('fieldsArray', 'fieldsJson'));
    }

    function datatable()
    {
        return BookingClass::datatable();
    }

    function create()
    {
        return view("pages.booking_class.form");
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            'class_name' => "required",
            'price' => "required",
        ]);

        $user = BookingClass::create($validated);
        if ($user) {
            return redirect(route('booking_class.index'));
        }
    }

    function edit(Request $request, $id)
    {
        $bookingClass = BookingClass::find($id);
        return view("pages.booking_class.form", compact('bookingClass'));
    }

    function update(Request $request, $id)
    {
        $validated = $request->validate([
            'class_name' => "required",
            'price' => "required",
        ]);

        $user = BookingClass::find($id);
        if ($user) {
            $user->update($validated);
            return redirect(route('booking_class.index'));
        }
    }

    function destroy($id)
    {
        BookingClass::find($id)->delete();
    }
}
