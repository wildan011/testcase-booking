<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function index()
    {
        $fields = ['id', 'name', 'role', 'phone', 'address', 'email', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.user.index", compact('fieldsArray', 'fieldsJson'));
    }

    function datatable()
    {
        return User::datatable();
    }

    function create()
    {
        return view("pages.user.form");
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            "name" => "required",
            "phone" => "required",
            "address" => "required",
            "role" => "required",
            "email" => "required|email|unique:users,email",
            "password" => "required|min:6",
        ]);

        $user = User::create($validated);
        if ($user) {
            return redirect(route('user.index'));
        }
    }

    function edit(Request $request, $id)
    {
        $user = User::find($id);
        return view("pages.user.form", compact('user'));
    }

    function update(Request $request, $id)
    {
        $validated = $request->validate([
            "name" => "required",
            "phone" => "required",
            "address" => "required",
            "role" => "required",
            "email" => "required|email|unique:users,email," . $id,
        ]);

        $user = User::find($id);
        if ($user) {
            $user->update($validated);
            return redirect(route('user.index'));
        }
    }

    function destroy($id)
    {
        User::find($id)->delete();
    }
}
