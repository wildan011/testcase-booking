<?php

namespace App\Http\Controllers\VehicleRoute;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use App\Models\BookingClass;
use App\Models\Vehicle;
use App\Models\VehicleRoute;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VehicleRouteController extends Controller
{
    function index()
    {
        $fields = ['id', 'vehicle', 'origin', 'destination', 'price', 'driver', 'Type Class', 'departure schedule', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.vehicle_route.index", compact('fieldsArray', 'fieldsJson'));
    }

    function datatable()
    {
        return VehicleRoute::with(['vehicle', 'bookingClass'])->datatable();
    }

    function create()
    {
        $vehicle = Vehicle::all();
        $bookingClass = BookingClass::all();
        return view("pages.vehicle_route.form", compact('vehicle', 'bookingClass'));
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            'vehicle_id' => "required",
            'price' => "required",
            'booking_class_id' => "required",
            'origin' => "required",
            'destination' => "required",
            'departure_schedule' => "required",
            'driver_name' => "required",
        ]);

        $explodeDate = explode("-", str_replace(" ", "", $request->get('departure_schedule')));
        $validated['departure_schedule_start'] = Carbon::parse($explodeDate[0])->format('Y-m-d');
        $validated['departure_schedule_end'] = Carbon::parse($explodeDate[1])->format('Y-m-d');

        $user = VehicleRoute::create($validated);
        if ($user) {
            return redirect(route('vehicle_route.index'));
        }
    }

    function edit(Request $request, $id)
    {
        $vehicle = Vehicle::all();
        $vehicleRoute = VehicleRoute::find($id);
        return view("pages.vehicle_route.form", compact('vehicleRoute', 'vehicle'));
    }

    function update(Request $request, $id)
    {
        $validated = $request->validate([
            'vehicle_id' => "required",
            'price' => "required",
            'booking_class_id' => "required",
            'origin' => "required",
            'destination' => "required",
            'departure_schedule' => "required",
            'driver_name' => "required",
        ]);

        $user = VehicleRoute::find($id);
        if ($user) {
            $user->update($validated);
            return redirect(route('vehicle_route.index'));
        }
    }

    function destroy($id)
    {
        VehicleRoute::find($id)->delete();
    }
}
