<?php

namespace App\Http\Controllers\DepartureSchedule;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use App\Models\Booking;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\VehicleRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DepartureScheduleController extends Controller
{
    function index()
    {
        $fields = ['id', 'vehicle', 'origin', 'destination', 'driver', 'Type Class', 'departure schedule', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.departure_schedule.index", compact('fieldsArray', 'fieldsJson'));
    }

    function add($id)
    {
        return view("pages.departure_schedule.form", compact('id'));
    }

    function datatable()
    {
        return VehicleRoute::with('vehicle')->datatable();
    }

    function view()
    {
        $vehicle = Vehicle::all();
        return view("pages.vehicle_route.view", compact('vehicle'));
    }

    function create()
    {
        $vehicle = Vehicle::all();
        return view("pages.vehicle_route.form", compact('vehicle'));
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            'chair' => "required",
            'transfer_via' => "required",
        ]);

        $user = User::find(Session::get('user_id'));
        $validated['user_id'] = $user->id;
        $validated['vehicle_route_id'] = $request->get('vehicle_route_id');
        $booked = Booking::create($validated);
        if ($booked) {
            return redirect(route('departure_schedule.view'));
        }
    }

    function edit(Request $request, $id)
    {
        $vehicle = Vehicle::all();
        $vehicleRoute = VehicleRoute::find($id);
        return view("pages.vehicle_route.form", compact('vehicleRoute', 'vehicle'));
    }

    function update(Request $request, $id)
    {
        $validated = $request->validate([
            'vehicle_id' => "required",
            'origin' => "required",
            'destination' => "required",
            'departure_schedule' => "required",
            'driver_name' => "required",
        ]);

        $user = VehicleRoute::find($id);
        if ($user) {
            $user->update($validated);
            return redirect(route('vehicle_route.index'));
        }
    }

    function destroy($id)
    {
        // dd($id);
        // $user = VehicleRoute::find($id)->delete();
        // return redirect('');
    }
}
