<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    function loginForm()
    {
        return view("auth.login");
    }

    function login(Request $request)
    {
        $validated = $request->validate([
            "email" => "required|email|exists:users,email",
            "password" => "required|min:6",
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $request->session()->put('user_id', $user->id);
            $request->session()->put('role', $user->role);
        }

        if ($user->role == "customer") {
            return redirect(route("departure_schedule.view"));
        }
        return redirect(route("user.index"));
    }

    function registerForm()
    {
        return view("auth.register");
    }

    function register(Request $request)
    {
        $validated = $request->validate([
            "name" => "required",
            "phone" => "required",
            "address" => "required",
            "email" => "required|email|unique:users,email",
            "password" => "required|min:6",
        ]);

        $validated['role'] = User::CUSTOMER;

        $user = User::create($validated);
        if ($user) {
            $user->createToken('auth_token')->plainTextToken;
            return redirect(route('login'));
        }
    }

    function logout(Request $request)
    {
        $request->session()->forget([
            'role',
            'user_id',
        ]);
        $request->session()->flush();
        return redirect('login');
    }
}
