<?php

namespace App\Http\Controllers\Vehicle;

use App\Http\Controllers\Controller;
use App\Http\Helper\Helper;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    function index()
    {
        $fields = ['id', 'name', 'tipe', 'plat', 'action'];
        $fieldsArray = Helper::convertToArray($fields);
        $fieldsJson = Helper::convertToJson($fields);
        return view("pages.vehicle.index", compact('fieldsArray', 'fieldsJson'));
    }

    function datatable()
    {
        return Vehicle::datatable();
    }

    function create()
    {
        return view("pages.vehicle.form");
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            "name" => "required",
            "type" => "required",
            "plat" => "required",
        ]);

        $user = Vehicle::create($validated);
        if ($user) {
            return redirect(route('vehicle.index'));
        }
    }

    function edit(Request $request, $id)
    {
        $vehicle = Vehicle::find($id);
        return view("pages.vehicle.form", compact('vehicle'));
    }

    function update(Request $request, $id)
    {
        $validated = $request->validate([
            "name" => "required",
            "type" => "required",
            "plat" => "required",
        ]);

        $user = Vehicle::find($id);
        if ($user) {
            $user->update($validated);
            return redirect(route('vehicle.index'));
        }
    }

    function destroy($id)
    {
        Vehicle::find($id)->delete();
    }
}
