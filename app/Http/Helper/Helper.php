<?php

namespace App\Http\Helper;

class Helper
{
    static function convertToJson($fields)
    {
        return collect($fields)->map(function ($query) {
            return ["data" => $query];
        })->toJson();
    }

    static function convertToArray($fields)
    {
        return collect($fields)->toArray();
    }
}
