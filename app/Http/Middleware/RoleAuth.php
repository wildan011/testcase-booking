<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleAuth
{
    public function handle(Request $request, Closure $next, $role)
    {
        if ($role == "customer") {
            return redirect(route("customer.home"));
        } else {
            return redirect(route("user.index"));
        }

        return $next($request);
    }
}
